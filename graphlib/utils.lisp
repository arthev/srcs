(in-package :graphlib)

(defun singlep (a)
  (and (consp a) (not (cdr a))))

(defun hash-keys (ht)
  (loop for key being the hash-keys of ht collect key))

(defun copy-hash-table (ht)
  (let ((nt (make-hash-table
             :test (hash-table-test ht)
             :rehash-size (hash-table-rehash-size ht)
             :rehash-threshold (hash-table-rehash-threshold ht)
             :size (hash-table-size ht))))
    (maphash (lambda (key val)
               (setf (gethash key nt) val))
             ht)
    nt))

(defmacro until (test &body body)
  `(do () (,test)
     ,@body))

(defmacro while (test &body body)
  `(do () ((not ,test))
     ,@body))

(defun tletw-bindings (bindings body)
  (let ((let-bs nil) (macrolet-bs nil))
    (dolist (b bindings)
      (if (eql (car b) :tfns)
          (dolist (tf (cdr b))
            (let ((test (when (listp tf) (cadr (member :test tf))))
                  (defa (when (listp tf) (cadr (member :default tf))))
                  (name (if (listp tf) (car tf) tf)))
              (push `(,name ,(append '(make-hash-table)
                                     (when test `(:test ,test))))
                    let-bs)
              (push `(,name (k) (list 'gethash k ',name ',defa))
                    macrolet-bs)))
          (push b let-bs)))
    `(,(reverse let-bs)
      (macrolet ,(reverse macrolet-bs)
        ,@body))))

(defmacro tlet (bindings &body body)
  `(let ,@(tletw-bindings bindings body)))

(defmacro tlet* (bindings &body body)
  `(let* ,@(tletw-bindings bindings body)))

(defmacro tfn-undef (arg tfn)
  `(remhash ,arg ,tfn))

(defclass queue ()
  ((head :initform nil)
   (tail :initform nil)))

(defmethod enqueue (data (q queue))
  (with-slots (head tail) q
    (if (endp head)
        (setf head (list data)
              tail head)
        (setf (cdr tail) (list data)
              tail (cdr tail)))))

(defmethod pequeue ((q queue))
  (with-slots (head) q
    (if (endp head)
        (values nil nil)
        (values (car head) t))))

(defmethod dequeue ((q queue))
  (multiple-value-bind (val found) (pequeue q)
    (when found
      (with-slots (head) q
        (setf head (cdr head))))
    (values val found)))
