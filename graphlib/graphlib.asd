;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(asdf:defsystem :graphlib
  :description "A graph drawing library"
  :version "0.0"
  :author "arthev"
  :licence "MIT License"
  :depends-on (:some-data-structures)
  :components ((:file "packages")
               (:file "utils")
               (:file "digraph")
               (:file "graphlib")))
