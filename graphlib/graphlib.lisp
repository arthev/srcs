;;;;Dictionary:
;;;;scc - strongly connected component
(in-package :graphlib)

;;;Pass 1: Optimal rank assignment
;;;1.a: break cycles
(defun find-sccs (g)
  "Tarjan's strongly connected components algorithm. O(n)."
  (tlet ((components nil) (i 0) (point-stack nil)
         (:tfns lowlink number stackp))
    (labels ((tarjan (v)
               (setf (lowlink v) (setf (number v) (incf i)))
               (push v point-stack)
               (setf (stackp v) t)
               (dolist (w (children-list v g))
                 ;;!number->tree arc, stackp->frond/cross arc.
                 (when (not (number w)) (tarjan w))
                 (when (or (not (number w)) (stackp w))
                   (setf (lowlink v) (min (lowlink v) (lowlink w)))))
               (when (= (lowlink v) (number v)) ;v is a component root
                 (let ((component nil))
                   (do ((w (car point-stack) (car point-stack)))
                       ((or (not w) (< (number w) (number v))) 'ok)
                     (push (pop point-stack) component)
                     (tfn-undef (car component) stackp))
                   (push component components)))))
      (dolist (v (node-list g)) (when (not (number v)) (tarjan v)))
      components)))
  
(defun break-component-cycles (component g ignorable)
  (tlet ((winner nil) (score 0)
         (:tfns in-component-p visited-p
                (cycle-count :default 0 :test #'equal)))
    (dolist (v component) (setf (in-component-p v) t))
    (labels ((counting-incf (e)
               (unless (gethash e ignorable)
                 (let ((new-score (incf (cycle-count e))))
                   (when (> new-score score)
                     (setf winner e score new-score)))))
             (counting-dfs (v)
               (setf (visited-p v) t)
               (dolist (w (children-list v g))
                 (when (in-component-p w)
                   (if (visited-p w)
                       (counting-incf (new-edge v w))
                       (counting-dfs w))))))
      (counting-dfs (car component)) ;scc, so all nodes are reachable
      (invert-edge winner g))))

(defun make-acyclic (g)
  (let ((scc (remove-if #'singlep (find-sccs g)))
        (ignorable (make-hash-table :test #'equal)))
    (until (endp scc)
      (tlet* ((c (pop scc))
              (dg (make-instance 'digraph))
              (:tfns in-component-p))
        (setf (gethash (break-component-cycles c g ignorable) ignorable) t)
        ;;Above 'fixes' one edge. Below creates a dg for efficiency,
        ;;and then finds new (smaller) sccs after the above fix.
        (dolist (v c) (setf (in-component-p v) t))
        (dolist (v c)
          (dolist (w (children-list v g))
            (when (in-component-p w) (add-edge dg v w))))
        (let ((nscc (remove-if #'singlep (find-sccs dg))))
          (dolist (c nscc)
            (push c scc)))))))

;;;;Next up: Making a fn to find a feasible tree for rank assignment.
;;;;Rank optimization problem:
;;;;Minimize: sum(w(v,w)*(rank(w) - rank(v)))
;;;;subj to:  rank(w) - rank(v) >= min-dist(v,w) for all e

;;;TODO: change parent-scans (and internal repr in digraph) to use hash
;;;      instead of list for children/parents.
;;;1.b: optimal rank assignment
(defun initial-rank (g)
  "Takes a DAG. Calculates & returns feas rank through implicit topological
   ordering. Intended for use by feasible-tree as part of solving the
   optimal rank assignment problem (pass 1)."
  (tlet ((scanned-nodes (make-instance 'queue))
         (:tfns rank)
         (parent-scans (make-hash-table)))
    (dolist (v (node-list g))
      (if (empty-edge-dir-p (parents v g) g)
          (enqueue v scanned-nodes)
          (setf (gethash v parent-scans) (copy-edge-dir (parents v g) g))))
    (until (null (pequeue scanned-nodes))
      ;;(u, v), (v, w)
      (let ((v (dequeue scanned-nodes))
            (r 0))
        (dolist (u (parents-list v g))
          (let ((nr (+ (rank u) (edge-min-length (new-edge u v) g))))
            (when (> nr r) (setf r nr))))
        (setf (rank v) r)
        (dolist (w (children-list v g))
          (remove-from-edge-dir v (gethash w parent-scans) g)
          (when (empty-edge-dir-p (gethash w parent-scans) g)
            (enqueue w scanned-nodes)))))
    rank))

(defun feasible-ranking-p (ranking g)
  (loop for e in (hash-keys (edges g))
        when (not (>= (- (gethash (edge-to e) ranking)
                         (gethash (edge-from e) ranking))
                      (edge-min-length e g)))
          do (return-from feasible-ranking-p nil))
  t)



;;;;TODO: I need a priority queue to progress
;; (defun tight-tree (rank g)
;;   (let ((fix-n (dolist (v (node-list g))
;;                  (when (zerop (gethash v rank)) (return v))))
;;         (tree (make-instance 'digraph))
;;         (:tfns in-tree-p))
;;     (do ((v-count (hash-table-count (g-children g))))
;;         ((= v-count (hash-table-count (g-children tree))) tree)
;;      ...)))

    
            
  

(defun feasible-tree (g)
  "Takes an acyclic graph and finds a feasible tree for rank assignment."
  (let* ((rank (initial-rank g))
         (tree (tight-tree rank g))
         ;;Unsure if (initial-cut-values (tree g)) is right signature
         (cut-values (initial-cut-values tree g)))
    (values rank tree cut-values)))













(defparameter ex (make-instance 'digraph))
(add-edge ex 'a 'b) (add-edge ex 'b 'e) (add-edge ex 'e 'a) (add-edge ex 'b 'f) (add-edge ex 'e 'f) (add-edge ex 'f 'g) (add-edge ex 'g 'f) (add-edge ex 'b 'c) (add-edge ex 'c 'd) (add-edge ex 'd 'c) (add-edge ex 'c 'g) (add-edge ex 'h 'g) (add-edge ex 'd 'h) (add-edge ex 'h 'd) 
