(in-package :graphlib)

;;;Edge section
(defun edge-to (e) (cdr e))

(defun edge-from (e) (car e))

(defun new-edge (from to) (cons from to))

;;;Graph section
(defclass digraph ()
  ((g-children
    :accessor g-children
    :initform (make-hash-table))
   (g-parents
    :accessor g-parents
    :initform (make-hash-table))
   (edges
    :accessor edges
    :initform (make-hash-table :test 'equal))))

(defmethod children ((v symbol) (g digraph))
  (gethash v (g-children g)))

(defmethod parents ((v symbol) (g digraph))
  (gethash v (g-parents g)))

(defmethod children-list ((v symbol) (g digraph))
  (hash-keys (children v g)))

(defmethod parents-list ((v symbol) (g digraph))
  (hash-keys (parents v g)))

(defmethod node-count ((g digraph)) 
  (hash-table-count (g-children g)))

(defmethod node-list ((g digraph))
  (hash-keys (g-children g)))

(defmacro def-edge-prop (meth-name prop)
  (let ((gs (gensym)))
    `(progn
       (defmethod ,meth-name (e (g digraph))
         (cdr (assoc ,prop (gethash e (edges g)))))
       (defmethod (setf ,meth-name) (,gs e (g digraph))
         (setf (cdr (assoc ,prop (gethash e (edges g)))) ,gs)))))

(defmacro def-edge-props (&rest rest)
  (if (evenp (length rest))
      `(progn ,@(loop for l = rest then (cddr l)
                      until (endp l)
                      collect `(def-edge-prop ,(car l) ,(cadr l))))
      (error "def-edge-props given odd args ~A." rest)))

(def-edge-props edge-weight :w edge-min-length :d edge-label :label)

(defmethod empty-edge-dir-p (edge-dir (g digraph))
  (zerop (hash-table-count edge-dir)))

(defmethod copy-edge-dir (edge-dir (g digraph))
  (copy-hash-table edge-dir))

(defmethod add-node ((v symbol) (g digraph))
  (when (or (nth-value 1 (gethash v (g-children g)))
            (nth-value 1 (gethash v (g-parents g))))
    (error "In add-node: non-unique node name '~A' given.~%" v))
  (setf (gethash v (g-children g)) (make-hash-table))
  (setf (gethash v (g-parents g)) (make-hash-table)))

(defmethod add-edge ((g digraph) (from symbol) (to symbol)
                     &key (w 1) (d 1) (label nil))
  (let ((edge (new-edge from to)))
    (when (nth-value 1 (gethash edge (edges g)))
      (error "In add-edge: non-unique edge '~A' given.~%" edge))
    (setf (gethash edge (edges g))
          (acons :w w (acons :d d (acons :label label nil))))
    (unless (nth-value 1 (gethash from (g-children g)))
      (add-node from g))
    (unless (nth-value 1 (gethash to (g-children g)))
      (add-node to g))
    (setf (gethash to (gethash from (g-children g))) t)
    (setf (gethash from (gethash to (g-parents g))) t)))

(defmethod remove-from-edge-dir (v edge-dir (g digraph))
  (remhash v edge-dir))

(defmethod remove-edge (edge (g digraph))
  (remhash edge (edges g))
  (remove-from-edge-dir (edge-to edge)
                        (gethash (edge-from edge) (g-children g))
                        g)
  (remove-from-edge-dir (edge-from edge)
                        (gethash (edge-to edge) (g-parents g))
                        g))

(defun invert-edge (edge g)
  (let* ((to (edge-to edge))
         (from (edge-from edge))
         (rev (new-edge to from))
         (org-weight (edge-weight edge g))
         (org-label (edge-label edge g))
         (org-min-length (edge-min-length edge g)))
    (if (nth-value 1 (gethash rev (edges g)))
        (let ((rev-weight (edge-weight rev g))
              (rev-min-length (edge-min-length rev g)))
          (setf (edge-weight rev g)
                (max rev-weight org-weight))
          (setf (edge-min-length rev g)
                (max org-min-length rev-min-length))
          (remove-edge edge g))
        (progn
          (remove-edge edge g)
          (add-edge g to from :w org-weight :d org-min-length
                              :label org-label)))))


