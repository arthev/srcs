(defparameter brute-string "~/src/challenges/010-password-generator/passwords.txt")
(setf *random-state* (make-random-state t))

(defun file->vector (filename)
  (with-open-file (fs filename :direction :input)
    (let ((line-vec (make-array 30 :fill-pointer 0 :adjustable t)))
      (loop for line = (read-line fs nil 'eof)
            until (eql line 'eof)
            do (vector-push-extend line line-vec))
      line-vec)))
      
(let* ((pwds (file->vector brute-string))
       (n (random (fill-pointer pwds))))
  (format t "Terrible password selected:~%~D: ~A~%" n (aref pwds n)))
