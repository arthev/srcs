(defun triangle-number (height)
  (/ (* height (1+ height)) 2))
