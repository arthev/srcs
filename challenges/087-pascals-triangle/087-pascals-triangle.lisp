;;From 054 - calculate binomial coefficients
(defun binomial-coefficient (n k)
  (when (> k n) 
    (error "binomial-coefficient called with k > n. n: ~D k: ~D~%" n k))
  (defun factorial (m)
    (if (zerop m) '(1) (loop for i from 1 to m collect i)))
  (let ((n! (factorial n))
	(k! (factorial k))
	(n-k! (factorial (- n k)))
	(long-div nil)
	(short-div nil))
    (if (> (length k!) (length n-k!))
	(progn (setf long-div k!) (setf short-div n-k!))
	(progn (setf long-div n-k!) (setf short-div k!)))
    (setf n! (subseq n! (length long-div))) ;Since elts are in sorted order
    (/ (apply #'* n!) (apply #'* short-div))))

(defun pascal-triangle (height)
  (let ((pad (length 
              (write-to-string 
               (binomial-coefficient height 
                                     (floor (/ height 2)))))))
    (loop for i from 0 below height
          do (loop for j from 0 to i
                   do (format t "~v,,@A " pad (binomial-coefficient i j)))
          do (format t "~%"))))
