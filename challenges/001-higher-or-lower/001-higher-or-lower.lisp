(defun prompt (prompt-sign)
  (format *query-io* "~a: " prompt-sign)
  (force-output *query-io*)
  (read-line *query-io*))

(defun initialize ()
  (random 1000001))

(defun looper (n)
  (loop for i from 0 to 20
     for guess = (parse-integer (prompt "Guess a number")) 
     then (parse-integer (prompt "Guess a number"))
     when (= guess n) do (return 'win)
     do (format *query-io* "No, it's not ~D. The number is ~A.~%" 
		guess (if (< n guess) "lower" "higher"))
     finally (return 'loss)))

(defun result (r)
  (if (equal r 'win)
      (format *query-io* "Congratulations! You guessed it!~%")
      (format *query-io* "Guess you didn't manage to guess it!~%"))
  (play (prompt "Play again? [y/n]")))

(defun play (choice)
  (if (member choice (list "n" "no" "NO" "n") :test #'equal)
      (progn
	(format *query-io* "Alright, bye!~%")
	'done)
      (progn 
	(format *query-io* "Alright! I'm thinking of a number between 0 and 1000000!~%")
	(result (looper (initialize))))))

(format *query-io* "Welcome to Higher or Lower!~%The amazing game where you guess which number I've selected...~%")
(play "y")

