(defclass kons-obj ()
  ((a
    :accessor kar
    :initform nil
    :initarg :val)
   (d
    :accessor kdr
    :initform nil
    :initarg :next)
   (i
    :accessor kir
    :initform nil
    :initarg :prev)))

(defmacro kons (x y &optional z)
  `(make-instance 'kons-obj :val ,x :next ,y :prev ,z))

(defmethod print-object ((obj kons-obj) out)
  (format out "~A" (cons (kar obj) (if (null (kdr obj))
                                       'NIX
                                       (kdr obj)))))

(defmethod initialize-instance :after ((obj kons-obj) &key)
  (when (and (not (typep (kdr obj) 'kons-obj))
             (not (null (kdr obj))))
    (setf (kdr obj) (kons (kdr obj) nil)))
  (when (typep (kdr obj) 'kons-obj)
    (setf (kir (kdr obj)) obj))
  (when (typep (kir obj) 'kons-obj)
    (setf (kdr (kir obj)) obj)))

(defun dll (&rest rest)
  (if (endp (cdr rest))
      (kons (car rest) nil)
      (kons (car rest) (apply #'dll (cdr rest)))))

(defun dll-lookup (lst i)
  (cond ((minusp i) (error "Dll-lookup received negative index: ~D.~%" i))
        ((zerop i) lst)
        (t (dll-lookup (kdr lst) (1- i)))))

(defun dll-length (lst)
  (loop while (not (null  lst))
        do (setf lst (kdr lst))
           count 1))

(defun dll-delete (elt)
  (let ((kire (kir elt))
        (kdre (kdr elt)))
    (when (not (null kdre))
      (setf (kir kdre) kire))
    (when (not (null kire))
      (setf (kdr kire) kdre))))
    
;;;;Insert is a trivial dispatch on :before or :after.

(defparameter a (kons 1 (kons 2 (kons 3 4))))
(defparameter b (dll 1 2 3 4 5))
