(in-package :markov-chain)

(defparameter *transition-matrix*
  (make-hash-table :test 'equal)
  "Entries are lists: (key num-of-entries a-list)
   where a-list is ((following-word1 num-of-entries1) (..2 ..2) ...)")

(defparameter *key-vector* 
  (make-array 0 :fill-pointer 0 :adjustable t)
  "A vector to store keys to *transition-matrix* for efficient
   random entries.")

(defparameter *vector-updated-p* nil
  "A simple flag.")
 
(defclass entry ()
  ((datum
    :accessor datum
    :initarg :datum)
   (occurrences
    :accessor occurrences
    :initform 0)
   (transitions
    :accessor transitions
    :initform nil))
  (:documentation "An entry for the transition matrix. Occurrences is
     a natural number counting the total of all numbers associated with
     all the entry's transitions. Transitions is an a-list consisting
     of (datum-of-some-other-entry . occurrences). So for text generation,
     simply count occurrences while digesting example text. For percentage
     based markov chains, just translate the percentages into a suitable
     spread. E.g. a coin flip:
     datum -> head/tail (two entries)
     occurrences -> 100 (100%)
     transitions -> '((head . 50) (tail . 50))
     fairly trivial example since coin flip can transition to both
     possible states from both possible states with same possibility..."))

(defmethod print-object ((obj entry) out)
  (format out "<Datum: ~S~%Occurrences: ~S~%Transitions: ~S>~%"
          (datum obj) (occurrences obj) (transitions obj)))

(defun list-of-words->list-of-pseudowords (lst)
  "Takes a list of strings (intended to be non-whitespace-containing words)
   and splits off any pre- or post-typography so those can be treated as
   'pseudowords' in their own right. Intent: Having the markov chain text
   generator generate suitable typography as well."
  (labels 
      ((pseudowordify (word)
         (let ((l (length word)))
           (cond ((zerop l) nil)
                 ((= 1 l) (list word))
                 (t (let ((pre (if (not (alphanumericp (char word 0))) 
                                   1 0))
                          (post (if (not (alphanumericp (char word (1- l))))
                                    1 0)))
                      (if (zerop (+ pre post))
                          (list word)
                          (append (pseudowordify 
                                   (subseq word 0 pre))
                                  (pseudowordify 
                                   (subseq word pre (- l post)))
                                  (pseudowordify
                                   (subseq word (- l post) l))))))))))
    (if (endp lst)
        nil
        (let* ((word (string-downcase (car lst)))
               (pseudowords (pseudowordify word)))
          (append pseudowords 
                  (list-of-words->list-of-pseudowords (cdr lst)))))))

(defun get-matrix-entry (datum)
  (gethash datum *transition-matrix*))

(defun create-matrix-entry (datum)
  (setf (gethash datum *transition-matrix*) 
        (make-instance 'entry :datum datum)))

(defun print-matrix ()
  (maphash (lambda (k v) (format t "K: ~S~%V: ~S~%" k v))
           *transition-matrix*))

(defun digest-datum (datum transition)
  (let ((matrix-entry (get-matrix-entry datum)))
    (when (not matrix-entry)
      (create-matrix-entry datum)
      (setf matrix-entry (get-matrix-entry datum)))
    (incf (occurrences matrix-entry))
    (let* ((transitions (transitions matrix-entry))
           (transition-entry (assoc transition
                                    transitions
                                    :test #'equal)))
      (if (not transition-entry)
          (setf (transitions matrix-entry)
                (acons transition 1 transitions))
          (incf (cdr transition-entry))))))

(defun digest-data (lst)
  (loop for sub on lst
        until (endp (cdr sub))
        do (digest-datum (car sub) (cadr sub))
        finally (return sub)))

(defun update-key-vector ()
  (unless *vector-updated-p*
    (let ((new-vector (make-array 0 :fill-pointer 0 :adjustable t)))
      (maphash (lambda (k v) (declare (ignorable v)) 
                 (vector-push-extend k new-vector))
               *transition-matrix*)
      (setf *key-vector* new-vector)
      (setf *vector-updated-p* t))))

(defun random-matrix-entry ()
  (unless *vector-updated-p* (update-key-vector))
  (get-matrix-entry (aref *key-vector*
                          (random (fill-pointer *key-vector*)))))


(defun chain-link (entry)
  (let ((n (random (occurrences entry)))
        (transitions (transitions entry)))
    (loop for e in transitions
          until (< n (cdr e))
          do (decf n (cdr e))
          finally (return (get-matrix-entry (car e))))))
    
(defun generate-chain (entry length)
  (unless *vector-updated-p* (update-key-vector))
  (let ((result nil))
    (loop repeat length
          for e = entry then (chain-link e)
          do (push (datum e) result))
    (nreverse result)))

(defun list-of-words->text (text)
  (if (endp (cdr text))
      (car text)
      (concatenate 'string (car text) " " 
                   (list-of-words->text (cdr text)))))

(defun generate-text (length)
  (let ((text-list (generate-chain (random-matrix-entry) length)))
    (list-of-words->text text-list)))

(defun str->data (str)
  (list-of-words->list-of-pseudowords (pu:split-on-whitespace str)))

(defun reset-chain ()
  (setf *transition-matrix* (make-hash-table :test 'equal))
  (setf *key-vector* (make-array 0 :fill-pointer 0 :adjustable t))
  (setf *vector-updated-p* nil))

(defun eat-string (str)
  (setf *vector-updated-p* nil)
  (digest-data (str->data str)))

(defun eat-file (filename)
  (with-open-file (stream filename :direction :input)
    (loop for leftover = "" then (car (eat-string 
                                       (concatenate 'string 
                                                    leftover " " line)))
          for line = (read-line stream nil 'eof)
          until (eql line 'eof))))
                                               
