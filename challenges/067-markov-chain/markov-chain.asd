;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(asdf:defsystem :markov-chain
  :name "babby's first markov chain"
  :version "0.0"
  :author "arthev"
  :depends-on (pathnames personal-utilities)
  :components ((:file "packages")
               (:file "markov-chain")))
