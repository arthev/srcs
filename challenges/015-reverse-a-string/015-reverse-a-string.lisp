;Built-in:
(defun reverse-string-b (s)
  (reverse s))

;Recursive
(defun reverse-string-r (s)
  (defun recurser (s)
    (let ((sl (length s)))
      (if (>= 1 sl)
	  s
	  (concatenate 'string
	   (subseq s (1- sl) sl)
	   (recurser (subseq s 0 (1- sl)))))))
  (recurser s))

;Iterative
(defun reverse-string-i (s)
  (loop for c across s
     for rs = (concatenate 'string (string c) "") 
     then (concatenate 'string (string c) rs)
     finally (if rs (return rs) (return ""))))
