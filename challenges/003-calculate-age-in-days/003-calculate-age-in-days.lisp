;Utility
(defun prompt (prompt-string)
  (format *query-io* "~a " prompt-string)
  (force-output *query-io*)
  (read))

;Program
(defun seconds-to-days (age-in-seconds)
  (floor (/ (- (get-universal-time) age-in-seconds)
	    (* 1 60 60 24))))

(defun age-in-days (&key (year nil) (month nil) (day nil))
  (unless year (setf year (prompt "Enter year of birth XXXX:")))
  (unless month (setf month (prompt "Enter month of birth (x)X:")))
  (unless day (setf day (prompt "Enter day of birth (x)X:")))
  (format *query-io* "You are ~D days old.~%"
	  (seconds-to-days (encode-universal-time 0 0 0 day month year))))

(when (< 1 (length *posix-argv*))
  (age-in-days))
