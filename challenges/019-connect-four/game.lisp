(in-package :connect-four)

(defun make-new-board (w h)
  (make-array `(,w ,h) :initial-element nil))

(defparameter *game-board* (make-new-board 8 8))

(defun other-player (player)
  (if (eql #\x player)
      #\o
      #\x))

(defun find-available-row (column)
  (loop for row from 0 below (array-dimension *game-board* 1)
        when (not (aref *game-board* column row))
          do (return-from find-available-row row)
        finally (return nil)))

(defun draw-p ()
  (loop for c from 0 below (array-dimension *game-board* 0)
        do (loop for r from 0 below (array-dimension *game-board* 1)
                 when (not (aref *game-board* c r))
                   do (return-from draw-p nil))
        finally (return t)))

(defun make-move (column player)
  (assert (< -1 column (array-dimension *game-board* 0))
          (column)
          "Column-number outside game board, expected: -1 < ~A < ~A"
          column (array-dimension *game-board* 0))
  (let ((row (find-available-row column)))
    (setf (aref *game-board* column row) player)
    (player-won-p column row player)))

(defun undo-move (column)
  (let ((ava (find-available-row column)))
    (format t "Ava: ~A~%" ava)
    (setf (aref *game-board* column (1- ava)) nil)))

(defun sum-along-direction-adjacent-to-slot (column row player x y)
  (let ((sum 0))
    (handler-case (loop for i from 1 
                        until (not (equal player (aref *game-board*
                                                       (+ column (* i x))
                                                       (+ row (* i y)))))
                        do (incf sum))
      (SB-INT:INVALID-ARRAY-INDEX-ERROR nil))
    (handler-case (loop for i from 1
                        until (not (equal player (aref *game-board*
                                                       (- column (* i x))
                                                       (- row (* i y)))))
                        do (incf sum))
      (SB-INT:INVALID-ARRAY-INDEX-ERROR nil))
    sum))

(defun player-won-p (column row player &key (faking-p nil))
  (let ((adjacents-p 
          (member 
           2 
           (loop for dir in '((0 1) (1 1) (1 0) (1 -1))
                 collect (sum-along-direction-adjacent-to-slot
                          column row player (car dir) (cadr dir)))
           :test #'<)))
    (if (not faking-p)
        (and adjacents-p (eql player (aref *game-board* column row)))
        adjacents-p)))

(defun terminal-win-p (player)
  (loop for column from 0 below (array-dimension *game-board* 0)
        do (loop for row from 0 below (array-dimension *game-board* 1)
                 for win-p = (player-won-p column row player)
                 when win-p
                   do (return-from terminal-win-p t))
        finally (return nil)))
