(in-package :connect-four)

(defparameter *transparent-black* (gk:vec4 0 0 0 0.75))

(defmacro with-text-bounds (vec)
  `(multiple-value-bind (origin width height advance)
       (gk:calc-text-bounds text (if font font *full-font*))
     ,(list 'gk:draw-text 'text vec 
            :fill-color '(if colour colour *colour*)
            :font '(if font font *full-font*))))

(defun center-text-at (text x y &key (font nil) (colour nil))
  (with-text-bounds
      (gk:vec2 (- x (/ width 2))
               (- y (/ height 2)))))

(defun right-align-text-at (text x y &key (font nil) (colour nil))
  (with-text-bounds
      (gk:vec2 (- x width)
               (- y (/ height 2)))))

(defun left-align-text-at (text x y &key (font nil) (colour nil))
  (with-text-bounds
      (gk:vec2 x
               (- y (/ height 2)))))

(defun draw-transparent-screen ()
  (gk:draw-rect *origin* *canvas-width* *canvas-height*
                :fill-paint *transparent-black*))
