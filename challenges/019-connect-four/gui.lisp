(in-package :connect-four)

(defparameter *origin* (gk:vec2 0 0))
(defparameter *canvas-width* 800)
(defparameter *canvas-height* 600)
(defparameter *black* (gk:vec4 0 0 0 1))
(defparameter *amber* (gk:vec4 (/ 255 255) (/ 176 255) (/ 0 255) 1))
(defparameter *red* (gk:vec4 (/ 255 255) (/ 25 255) (/ 25 255) 1))
(defparameter *blue* (gk:vec4 (/ 25 255) (/ 25 255) (/ 255 255) 1))
(defparameter *bz* 64)

(defparameter *active-game* nil)
(defparameter *cursor-pos* *origin*)
(defparameter *players* #(#\x #\o))
(defparameter *winner* nil)
(defparameter *timer* 0)

(defparameter *1-player-mopt* 
  (list "1-player" 
        (gk:vec2 (/ *canvas-width* 2) 
                 (- *canvas-height* 300))
        (lambda () (setf *active-game* (create-game 1)))))
(defparameter *2-player-mopt* 
  (list "2-player" 
        (gk:vec2 (/ *canvas-width* 2) 
                 (- *canvas-height* 400))
        (lambda () (setf *active-game* (create-game 2)))))                  
(defparameter *exit-mopt* 
  (list "Exit" (gk:vec2 (/ *canvas-width* 2) 
                        (- *canvas-height* 500))
        (lambda () (gk:stop))))

(defparameter *mopt-list* 
  (list *1-player-mopt* *2-player-mopt* *exit-mopt*))

(defun over-opt-p (opt)
  (< (abs (- (gk:y *cursor-pos*) (gk:y (cadr opt)))) 50))

(defun create-game (num-players)
  (list (cons :num-players num-players)
        (cons :board (make-new-board 8 8))
        (cons :i (random 2))))

(gk:defgame connect-four-kit ()
  ()
  (:viewport-width *canvas-width*)
  (:viewport-height *canvas-height*)
  (:viewport-title "Connect Four"))

(gk:register-resource-package 
 :connect-four
 (asdf:system-relative-pathname
  :connect-four-gui "assets/"))

(gk:define-font 'connect-four::mono-font "LiberationMono-Regular.ttf")

(defmethod gk:draw ((game connect-four-kit))
  (gk:draw-rect *origin* *canvas-width* *canvas-height* :fill-paint *black*)
  (if (not *active-game*)
      (draw-menu)
      (progn (draw-game)
             (when *winner*
               (display-win-screen *winner*)))))

(defun draw-menu ()
  (center-text-at "Connect Four" 
                  (/ *canvas-width* 2) 
                  (- *canvas-height* 100)
                  :font *double-font* :colour *amber*)
  (loop for opt in *mopt-list*
        for col = (if (over-opt-p opt)
                      *red* *amber*)
        do (center-text-at (car opt) (gk:x (cadr opt)) (gk:y (cadr opt))
                           :colour col)))
(defun start-timer ()
  (setf *timer* 100))

(defmethod gk:act ((game connect-four-kit))
  (when (and *active-game* (not *winner*))
    (let ((*game-board* (cdr (assoc :board *active-game*))))
      (cond ((draw-p) 
             (setf *winner* "A draw!") 
             (start-timer))
            ((and (= 1 (cdr (assoc :num-players *active-game*)))
                  (plusp (cdr (assoc :i *active-game*))))
             (let ((win-p (make-move (get-computer-move #\o) #\o)))
               (setf (cdr (assoc :i *active-game*)) 0)
               (when win-p
                 (setf *winner* "The red player won!")
                 (start-timer))))
            (t nil)))))

(defun game-details ()
  (let* ((gb (cdr (assoc :board *active-game*)))
         (w (array-dimension gb 0))
         (h (array-dimension gb 1))
         (hoffset (/ (- *canvas-width* (* w *bz*)) 2))
         (x (gk:x *cursor-pos*))
         (cursor-column (floor (/ (- x hoffset) *bz*)))
         (player (aref *players* (cdr (assoc :i *active-game*)))))
    (values gb w h hoffset x cursor-column player)))

(defun player->colour (player)
  (cond ((eql #\x player) *blue*)
        ((eql #\o player) *red*)
        (t nil)))

(defun draw-game ()
  (multiple-value-bind (gb w h hoffset x cursor-column player) 
      (game-details)
    (loop for i from 0 to w
          do (gk:draw-line (gk:vec2 (+ hoffset (* i *bz*)) 0)
                           (gk:vec2 (+ hoffset (* i *bz*)) (* h *bz*))
                           *amber*
                           :thickness 2))
    (loop for i from 0 below h
          do (gk:draw-line (gk:vec2 hoffset (+ (* i *bz*) 2))
                           (gk:vec2 (- *canvas-width* hoffset) 
                                    (+ (* i *bz*) 2))
                           *amber*
                           :thickness 2))
    (loop for c from 0 below w
          do (loop for r below h
                   for col = (let ((v (aref gb c r)))
                               (player->colour v))
                   when col
                     do (gk:draw-circle 
                         (gk:vec2 (+ hoffset 
                                     (* c *bz*) (/ *bz* 2))
                                  (+ 2 (* r *bz*) (/ *bz* 2)))
                         (- (/ *bz* 2) 1)
                         :fill-paint col)))
    (when (<= hoffset x (- *canvas-width* hoffset))
      (gk:draw-circle
       (gk:vec2 (+ hoffset (* cursor-column *bz*) (/ *bz* 2))
                (+ 2 (* h *bz*) (/ *bz* 2)))
       (- (/ *bz* 2) 1)
       :fill-paint (player->colour player)))))

(defun mouse-fn ()
  (if (not *active-game*)
      (click-menu)
      (click-game)))

(defun click-game ()
  (when *winner* (return-from click-game nil))
  (multiple-value-bind (gb w h hoffset x column player) 
      (game-details)
    (declare (ignorable h hoffset x))
    (let ((*game-board* gb)
          (i (cdr (assoc :i *active-game*))))
      (when (and (<= 0 column (1- w)) (find-available-row column))
        (let ((win-p (make-move column player)))
          (setf (cdr (assoc :i *active-game*))
                (if (zerop i) 1 0))
          (when win-p
            (setf *winner* (format nil "The ~A player won!"
                                   (if (eql player #\x) "blue" "red")))
            (start-timer)))))))

(defun display-win-screen (str)
  (draw-transparent-screen)
  (center-text-at str
                  (/ *canvas-width* 2)
                  (/ *canvas-height* 2)
                  :font *full-font*
                  :colour *amber*)
  (if (plusp *timer*)
      (decf *timer*)
      (progn (setf *winner* nil)
             (setf *active-game* nil))))

(defun click-menu ()
  (loop for opt in (list *1-player-mopt* *2-player-mopt* *exit-mopt*)
        when (over-opt-p opt)
          do (funcall (caddr opt))))
        
(defun main ()
  (gk:start 'connect-four-kit))

(defmethod gk:post-initialize ((game connect-four-kit))
  (defparameter *full-font* 
    (gk:make-font 'connect-four::mono-font *bz*))
  (defparameter *half-font* 
    (gk:make-font 'connect-four::mono-font (/ *bz* 2)))
  (defparameter *double-font*
    (gk:make-font 'connect-four::mono-font (* 2 *bz*)))
  (gk:bind-button :mouse-left :pressed 
                  #'mouse-fn)
  (gk:bind-cursor (lambda (x y) (if (and x y)
                                    (setf *cursor-pos* (gk:vec2 x y))
                                    (setf *cursor-pos* *origin*)))))
