(in-package :connect-four)

;;In contrast to get-human-move, get-computer-move is AI and simulation
;;and therefore part of game simulation rather than UI.
(defun get-human-move ()
  (loop (let ((input (pu:prompt-i ">")))
          (when (and (numberp input) (not (minusp input))
                     (< input (array-dimension *game-board* 0))
                     (find-available-row input))
            (return-from get-human-move input)))))

(defun play-logic-move-function (num-players)
  (let ((players #(#\x #\o))
        (i (random 2)))
    (lambda ()
      (let* ((player (aref players i))
             (computer-p (and (= num-players 1) (eql player #\o))))
        (if (zerop i) (setf i 1) (setf i 0))
        (format t "Player ~A's turn: [0-~A]~%"
                player
                (1- (array-dimension *game-board* 0)))
        (if computer-p
            (values (get-computer-move player) player)
            (values (get-human-move) player))))))

(defun game-loop (move-fn printer-fn)
  (let ((*game-board* (make-new-board 8 8)))
    (loop for (column player) = (multiple-value-list (funcall move-fn))
          for win-p = (make-move column player)
          do (funcall printer-fn player column)
          until (or win-p (draw-p))
          finally (if win-p 
                      (print-winner player)
                      (print-draw)))))
                      
(defun print-move (player column)
  (format t "Player ~A went with column ~A.~%" player column))

(defun print-game-board (&rest junk)
  (declare (ignorable junk))
  (loop for row downfrom (1- (array-dimension *game-board* 1)) to 0
        do (format t "|~{~A|~}~%" 
                   (loop for column from 0 below 
                                           (array-dimension *game-board* 0)
                         collect (or (aref *game-board* column row)
                                     #\ )))))


(defun print-winner (player)
  (format t "Player ~A won!~%" player))

(defun print-draw ()
  (format t "A draw!~%"))

(defun main ()
  (loop (format t "~{~A~%~}" (list "Select an option:" 
                                   "1) 1-player"
                                   "2) 2-player"
                                   "3) 1-player Mental Mode"
                                   "4) 2-player Mental Mode"
                                   "5) Exit"))
        (let ((option (pu:prompt-i ">")))
          (case option
            (1 
             (print-game-board)
             (game-loop (play-logic-move-function 1) #'print-game-board))
            (2 
             (print-game-board)
             (game-loop (play-logic-move-function 2) #'print-game-board))
            (3 (game-loop (play-logic-move-function 1) #'print-move))
            (4 (game-loop (play-logic-move-function 2) #'print-move))
            (5 (return-from main 'finished))))))
