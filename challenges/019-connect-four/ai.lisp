(in-package :connect-four)

(defun get-computer-move ()
  (loop for r = (random (array-dimension *game-board* 0))
        until (find-available-row r)
        finally (return r)))
