;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(asdf:defsystem :connect-four-tui
  :name "A connect-four game with TUI"
  :version "0.0"
  :author "arthev"
  :depends-on (pathnames personal-utilities)
  :components ((:file "packages-tui")
               (:file "ai")
               (:file "game")
               (:file "tui")))
