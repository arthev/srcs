(defpackage :connect-four 
  (:use :cl)
  (:local-nicknames (:pathnames :com.gigamonkeys.pathnames-system)
                    (:pu :cl-arthur))
  (:export :main))
