;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(pushnew :bodge-gl2 *features*)
(asdf:defsystem :connect-four-gui
  :name "A connect-four game with GUI"
  :version "0.0"
  :author "arthev"
  :depends-on (pathnames personal-utilities trivial-gamekit)
  :components ((:file "packages-gui")
               (:file "ai")
               (:file "game")
               (:file "gk-utils")
               (:file "gui")))
