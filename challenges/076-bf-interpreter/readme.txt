A simple, non-optimized brainfuck interpreter. Around five hours of work. 212 LOCs. Written on 07.09.18, by Arthur Evensen.
To run compiled fasl from CLI:
PROG -program SRC -input INPUT
both SRC and INPUT can be given as "strings". 
[]'s may need to be escaped into \[ \] for assorted program externals to manage to read.
'Tested' on the test programs.

Some possible improvements:
* Add interactive mode support
* Optimizations
* Improve the code
