;;;utility
(defun file-get-contents (filename)
  "http://sodaware.sdf.org/notes/cl-read-file-into-string/"
  (with-open-file (stream filename)
    (let ((contents (make-string (file-length stream))))
      (read-sequence contents stream)
      contents)))

;;;program
(load "~/quicklisp/setup.lisp")
(ql:quickload "pathnames" :silent t) ;This is Peter Seibel's pathnames library from Practical Common Lisp

(defconstant +max-cell-value+ (1- (expt 2 8)))
(defconstant +min-cell-value+ 0)
(defparameter *instruction-list* "+-><.,[]")

(defun make-new-tape ()
  "Tape for one side of the infinite left-right memory of a bf."
  (let ((new-tape (make-array 30 :fill-pointer 0 :adjustable t)))
    (loop repeat 2 do (vector-push-extend 0 new-tape))
    new-tape))

(defclass bf ()
  ((output-mode
    :accessor output-mode
    :initform 'ascii
    :initarg :output-mode)
   (left-tape
    :accessor left-tape
    :initform (make-new-tape)
    :initarg :left-tape)
   (right-tape
    :accessor right-tape
    :initform (make-new-tape)
    :initarg :right-tape)
   (pc
    :accessor pc
    :initform 0
    :initarg :pc)
   (head
    :accessor head
    :initform 0
    :initarg :head)
   (program
    :accessor program
    :initform nil
    :initarg :program)
   (input
    :accessor input
    :initform nil
    :initarg :input))
  (:documentation "A brainfuck-machine-program-class."))

(defun left-cell-num (head)
  "Head->left-tape vector index. 1- since head<-0 in right-tape."
  (1- (abs head)))

(defmacro tape-cell-access (bf &key (v nil v-supplied-p))
  (format *query-io* "Hereth v: ~A~%" v)
  (let ((left '(aref (left-tape bf) (left-cell-num head)))
        (right '(aref (right-tape bf) head)))
    `(let ((head (head ,bf)))
       (if (minusp head)
           ,(if v-supplied-p `(setf ,left ,v) left)
           ,(if v-supplied-p `(setf ,right ,v) right)))))

(defun tape-cell (bf)
  (tape-cell-access bf))

(defun (setf tape-cell) (value bf)
  (tape-cell-access bf :v value))

(defun potential-extend (direction-p head tape)
  "Extend a tape if direction-p and out-of-fills."
  (when (and direction-p
             (>= head (fill-pointer tape)))
    (vector-push-extend 0 tape)))

(defun potential-right-extend (bf)
  "Extend the infinite right-tape if head points out-of-bounds."
  (let ((head (head bf)))
    (potential-extend (plusp head) head (right-tape bf))))

(defun potential-left-extend (bf)
  "Extend the infinite left-tape if head points out-of-bounds."
  (let ((head (head bf)))
    (potential-extend (minusp head) (left-cell-num head) (left-tape bf))))

(defmacro update-cell (bf predicate consequent alternative)
  `(let* ((old-val (tape-cell ,bf))
          (new-val (if ,predicate ,consequent ,alternative)))
     (declare (ignorable old-val))
     (setf (tape-cell ,bf) new-val)))

(defun increment-cell (bf)
  (update-cell bf (>= old-val +max-cell-value+) 
               +min-cell-value+ (1+ old-val)))

(defun decrement-cell (bf)
  (update-cell bf (<= old-val +min-cell-value+) 
               +max-cell-value+ (1- old-val)))

;(declaim #+sbcl(sb-ext:muffle-conditions style-warning))
(defun input-cell (bf)
  (update-cell bf (listen (input bf)) 
               (char-code (read-char (input bf))) 0))
;(declaim #+sbcl(sb-ext:unmuffle-conditions style-warning))

(defun output-cell (bf)
  "Outputs the value of the current cell."
  (let ((print-datum (output-mode-format (tape-cell bf) 
                                         (output-mode bf))))
    (format *query-io* "~A" print-datum)
    print-datum))

(defun output-mode-format (cell-value output-mode)
  "Interprets cell-value according to the bf's output-mode."
  (cond ((eql output-mode 'ascii)
         (code-char cell-value))
        (t (error "~A is not a valid output-mode." output-mode))))

(defun string->program (program-string)
  "Turns a string into a program vector."
  (let ((program-vector (make-array 30 :fill-pointer 0 :adjustable t)))
    (loop for c across program-string
          when (find c *instruction-list*)
            do (vector-push-extend c program-vector))
    program-vector))

(defun parse (pot-file-string)
  "Returns the corresponding program vector, whether from file or string."
  (let ((pot-file-input (com.gigamonkeys.pathnames:file-exists-p pot-file-string)))
    (string->program 
     (if (null pot-file-input)
         pot-file-string
         (file-get-contents pot-file-input)))))

(defun search-step (pc-s program depth result matching)
  "Step action for find-matching-*. Note that when going backwards,
   negative depth accrues rather than positive."
  (assert (< pc-s (length program)) (pc-s)
          "~S = ~A, should be an int in [0, ~A]." 
          'pc-s pc-s (1- (length program)))
  (cond ((and (zerop depth) (eql (aref program pc-s) matching))
         (funcall result pc-s))
        ((eql (aref program pc-s) #\])
         (1- depth))
        ((eql (aref program pc-s) #\[)
         (1+ depth))
        (t depth)))

(defun find-matching-] (pc program)
  "Linear search until match found. Error if no match."
  (flet ((result (pc) (return-from find-matching-] pc)))
    (loop with depth = 0
          for i from (1+ pc) below (length program)
          do (setf depth (search-step i program depth #'result #\]))
          finally (error "~Dth instruction: [. Matching ] not found, end-of-program with depth ~D.~%" pc depth))))

(defun find-matching-[ (pc program)
  "Linear search until match found. Error if no match."
  (flet ((result (pc) (return-from find-matching-[ pc)))
    (loop with depth = 0
          for i from (1- pc) downto 0
          do (setf depth (search-step i program depth #'result #\[))
          finally (error "~Dth instruction: ]. Matching [ not found, start-of-program with depth ~D.~%" pc depth))))

(defun jump-if-zero (bf)
  "Returns value so brainfuck-interpreter can update pc as required."
  (if (zerop (tape-cell bf))
      (find-matching-] (pc bf) (program bf))
      (pc bf)))

(defun jump-unless-zero (bf)
  "Returns value so brainfuck-interpreter can update pc as required."
  (if (plusp (tape-cell bf))
      (find-matching-[ (pc bf) (program bf))
      (pc bf)))

(defun brainfuck-interpret (instruction bf)
  (case instruction
    (#\> (incf (head bf)) (potential-right-extend bf))
    (#\< (decf (head bf)) (potential-left-extend bf))
    (#\+ (increment-cell bf))
    (#\- (decrement-cell bf))
    (#\. (output-cell bf))
    (#\, (input-cell bf))
    (#\[ (setf (pc bf) (jump-if-zero bf)))
    (#\] (setf (pc bf) (jump-unless-zero bf)))
    (otherwise 'ignore)))

(defun input-stream-potentially-file (input)
  "Returns corresponding input stream, whether from file or not."
  (when input
    (let ((pot-file-input 
            (com.gigamonkeys.pathnames:file-exists-p input)))
      (if (null pot-file-input)
          (make-string-input-stream input)
          (open pot-file-input :direction :input)))))

(defun execute (program &key (input nil))
  "Creates a bf instance and runs corresponding program (& input) on it."
  (let* ((bf (make-instance 'bf 
                            :program program 
                            :input (input-stream-potentially-file input)))
         (program-length (length (program bf))))
    (loop while (< (pc bf) program-length)
          do (brainfuck-interpret (aref (program bf) (pc bf)) bf)
          do (incf (pc bf)))))

(defun main (argv)
  "Handles command line arguments and invokes execute as appropriate."
  (let ((argv-alist '()))
    (pop argv) ;First arg is file name of bf-interpreter, so garbage.
    (when (oddp (length argv)) 
      (error "MF launch: Odd number of argwords."))
    (loop while (not (endp argv))
          do (setf argv-alist (acons (pop argv) (pop argv) argv-alist)))
    (let ((-p (or (assoc "-p" argv-alist :test #'equal)
                  (assoc "--program" argv-alist :test #'equal)))
          (-i (or (assoc "-i" argv-alist :test #'equal)
                  (assoc "--input" argv-alist :test #'equal))))
      (cond ((and -p -i) (execute (parse (cdr -p))
                                  :input (cdr -i)))
            (-p (execute (parse (cdr -p))))
            (t (error "MF launch: BF -p PATH/STRING (-i PATH/STRING)"))))))

;;;Presumably, this will only trigger when called with args from terminal:
#+sbcl (when (> (length *posix-argv*) 1)
         (main *posix-argv*))
