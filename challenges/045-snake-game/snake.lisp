(in-package :snake-game)

(defclass snake ()
  ((tail
    :accessor tail
    :initform (list (gk:vec2 (fhalf *mw*) (fhalf *mh*)) 
                    (gk:vec2 (fhalf *mw*) (1- (fhalf *mh*)))
                    (gk:vec2 (fhalf *mw*) (+ (- 2) (fhalf *mh*)))))
   (head
    :accessor head
    :initarg :head)
   (direction
    :accessor direction
    :initform :down)
   (turning-direction
    :accessor turning-direction
    :initform nil)))

;;;Push, pop and peek on turning-direction implement a basic queue
(defun push-turning-direction (snake dir)
  (if (null (turning-direction snake))
      (setf (turning-direction snake) (list dir))
      (push dir (turning-direction snake))))

(defun pop-turning-direction (snake)
  (cond ((null (turning-direction snake)) nil)
        ((= 1 (length (turning-direction snake)))
         (pop (turning-direction snake)))
        (t
         (let* ((l (length (turning-direction snake)))
                (elt (nth (1- l) (turning-direction snake))))
           (setf (cdr (nthcdr (- l 2) (turning-direction snake))) nil)
           elt))))

(defun peek-turning-direction (snake)
  (cond ((null (turning-direction snake)) nil)
        (t (car (last (turning-direction snake))))))

(defun reinit-snake-head (s)
  (reinitialize-instance s :head (car (last (tail s)))))

(defmethod initialize-instance :after ((s snake) &key)
  (reinit-snake-head s))

(defun update-direction (snake)
  (let ((peek (peek-turning-direction snake)))
    (cond ((equal (direction snake) (opposite-direction peek))
           (pop-turning-direction snake) (update-direction snake))
          ((null peek) nil)
          (t (setf (direction snake) (pop-turning-direction snake))))))

(defmethod snake-growth ((s snake))
  (setf (tail s) (cons (gk:vec2 -1 -1) (tail s))))

(defun move-snake (snake)
  (macrolet
      ((wrapf (spot pred then else)
         `(if ,(append pred `(,spot))
              (setf ,spot ,then)
              (,else ,spot))))
    (loop for segment on (tail snake)
          until (endp (cdr segment))
          do (progn (setf (gk:x (car segment)) 
                          (gk:x (cadr segment)))
                    (setf (gk:y (car segment))
                          (gk:y (cadr segment))))
          finally
             (case (direction snake)
               (:up (wrapf (gk:y (car segment)) (<= *mh*) 0 incf))
               (:down (wrapf (gk:y (car segment)) (>= 0) *mh* decf))
               (:left (wrapf (gk:x (car segment)) (>= 0) *mw* decf))
               (:right (wrapf (gk:x (car segment)) (<= *mw*) 0 incf))
               (otherwise
                (error "Non-dir snake dir: ~A~%" 
                       (direction snake)))))))

(defmethod draw ((s snake))
  (loop for segment in (tail s)
        do (gk:draw-rect 
            (gk:vec2 (1+ (* (gk:x segment) *bz*))
                     (1+ (* (gk:y segment) *bz*)))
            (1- *bz*) (1- *bz*) :fill-paint *colour*))
  
  (when (direction s)
    (let* ((fbz (/ *bz* 4))
           (hc (gk:vec2 (+ (half *bz*) (* *bz* (gk:x (head s))))
                        (+ (half *bz*) (* *bz* (gk:y (head s))))))
           (dist (position (direction s) '(:up :right :down :left))))
      (loop repeat (1+ dist)
            for first-eye = (gk:vec2 (- fbz) fbz)
              then (gk:vec2 (gk:y first-eye) 
                            (- (gk:x first-eye)))
            for second-eye = (gk:vec2 fbz fbz)
              then (gk:vec2 (gk:y second-eye) 
                            (- (gk:x second-eye)))
            finally
               (progn
                 (gk:draw-circle (gk:add hc first-eye)
                                 (half fbz) :fill-paint *black*)
                 (gk:draw-circle (gk:add hc second-eye)
                                 (half fbz) :fill-paint *black*))))))
