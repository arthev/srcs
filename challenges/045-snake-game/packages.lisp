(defpackage :snake-game
  (:use :cl)
  (:local-nicknames (:gk :gamekit))
  (:export start-game))

;;;For debugging
;(log:config :sane2)
