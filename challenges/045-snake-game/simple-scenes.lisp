(in-package :snake-game)

(defclass game-over-scene (base-scene)
  ((scount
    :initform 0)))

(defmethod scount ((scene game-over-scene))
  (incf (slot-value scene 'scount)))

(defmethod draw ((scene game-over-scene))
  (enforce-framerate)
  (let* ((segment (/ (scount scene) *difficulty*))
         (blinkp (<= 0.5 (mod segment 1))))
    (draw (cadr *scene-stack*))
    (if (> segment 3)
        (pop-scene)
        (when blinkp (draw-transparent-screen)))))

(defmethod when-popped ((scene game-over-scene))
  (pop-scene))

(defclass high-score-scene (base-scene)
  ())

(defmethod draw ((scene high-score-scene))
  (let ((base-voffset (* 3 *bz*))
        (margin (* 1.5 *bz*))
        (mw (half *canvas-width*)))
    (center-text-at "Top 5" mw (- *canvas-height* *bz*) *double-font*)
    (loop for i from 0 below (length *high-scores*)
          do (let ((elem (nth i *high-scores*))
                   (voff (- *canvas-height* base-voffset (* i margin))))
               (center-text-at ":" mw voff)
               (right-align-text-at (string (car elem)) (- mw *bz*) voff)
               (left-align-text-at (write-to-string (cdr elem)) 
                                   (+ mw *bz*) voff)))))

(defclass pause-scene (base-scene)
  ())

(defmethod draw ((scene pause-scene))
  (draw (cadr *scene-stack*))
  (draw-transparent-screen)
  (center-text-at "Paused! (Press enter)"
                  (half *canvas-width*)
                  (half *canvas-height*)))


