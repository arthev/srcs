(in-package :snake-game)

(defparameter *scene-stack* nil)

(defclass base-scene ()
  ((bound-keys
   :accessor bound-keys
   :initform nil))
  (:documentation "Base scene."))

(defun pop-scene ()
  (let* ((popped (pop *scene-stack*))
         (after-pop *scene-stack*))
    (unbind-keys popped)
    (when-popped popped)
    (unless (or (endp *scene-stack*) 
                (not (eql (car after-pop) (car *scene-stack*))))
      (bind-keys (car *scene-stack*)))))

(defmethod when-popped ((scene base-scene))
  'do-nothing)

(defmethod push-scene ((scene base-scene))
  (unless (null *scene-stack*)
    (unbind-keys (car *scene-stack*)))
  (push scene *scene-stack*)
  ;No #'when-pushed necessary, since 'make-instance :after allows init.
  (bind-keys scene))

(defmethod act ((scene base-scene))
  'do-nothing)

(defmethod draw ((scene base-scene))
  'do-nothing)

(defmethod bind-keys ((scene base-scene))
  (bind-button :enter :pressed #'pop-scene))

(defun unbind-keys (scene)
  (mapc (lambda (b) (bind-button b :pressed (lambda ())))
        (bound-keys scene))
  (setf (bound-keys scene) nil))
