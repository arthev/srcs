(in-package :snake-game)

(defparameter *text-height* (- *canvas-height* (* 0.75 *hbz*)))

(defparameter *difficulty* 10)
(defparameter *time-per-frame* 
  (/ INTERNAL-TIME-UNITS-PER-SECOND *difficulty*))
(defparameter *last-time* 0)

(defparameter *opposite-direction* '((:up . :down) (:down . :up)
                                     (:left . :right) (:right . :left)))

(defun opposite-direction (direction)
  (cdr (assoc direction *opposite-direction*)))

(defclass snake-scene (base-scene)
  ((snake
    :accessor snake
    :initform (make-instance 'snake))
   (apple
    :accessor apple
    :initform (make-instance 'apple))
   (score
    :accessor score
    :initform 0))
  (:documentation "A scene for the snake game itself."))

(defun board-cleared (scene)
  (incf (score scene) (* 100 *difficulty*))
  (setf (snake scene) (make-instance 'snake))
  (setf (apple scene) (make-instance 'apple))
  (incf *difficulty* 3)
  (setf *time-per-frame* (/ INTERNAL-TIME-UNITS-PER-SECOND *difficulty*)))

(defmethod when-popped ((scene snake-scene))
  (let ((beatens (member (score scene) *high-scores* :key #'cdr :test #'>)))
    (unless (null beatens)
      (push-scene (make-instance 'high-score-entry-scene 
                                 :score (score scene))))))

(defun winp (snake)
  (= (* (1+ *mw*) (1+ *mh*))
     (length (tail snake))))

(defun losep (snake)
  (vec-in-vec-list (head snake) (tail snake)))
  
(defun enforce-framerate ()
  "A basic timer running off of some dynamic variables"
  (let* ((time (get-internal-real-time))
         (diff (- time *last-time*)))
    (when (< diff *time-per-frame*)
      (sleep (/ (- *time-per-frame* diff) INTERNAL-TIME-UNITS-PER-SECOND)))
    (setf *last-time* (get-internal-real-time))))

(defun grow-and-relocate-apple-if-eaten (scene)
  (let* ((s (snake scene))
         (a (apple scene))
         (h (car (last (tail s)))))
    (when (vec2-equal (location a) h)
      (snake-growth s)
      (relocate-apple a s)
      (add-score scene))))

(defmethod act ((scene snake-scene))
  (enforce-framerate)
  (update-direction (snake scene))
  (move-snake (snake scene))
  (grow-and-relocate-apple-if-eaten scene)
  (when (winp (snake scene))
    (board-cleared scene))
  (when (losep (snake scene))
    (push-scene (make-instance 'game-over-scene))))

(defmethod add-score ((scene snake-scene))
  (incf (score scene) (+ 10 (/ *difficulty* 10)))) 
               
(defmethod draw ((scene snake-scene))
  (draw (snake scene))
  (draw (apple scene))
  (gk:draw-line (gk:vec2 0 (+ (* *bz* (1+ *mh*)) 2))
                (gk:vec2 (* *bz* (1+ *mw*))
                         (+ (* *bz* (1+ *mh*)) 2))
                *colour*
                :thickness 2)
  (left-align-text-at (format nil "Score: ~A" (round (score scene)))
                      0 *text-height*))

(defmethod bind-keys ((scene snake-scene))
  (mapc (lambda (b) 
          (bind-button 
           b :pressed
           (lambda ()
             (push-turning-direction (snake scene) b))))
        '(:up :down :left :right))
  (bind-button :p :pressed 
               (lambda () (push-scene (make-instance 'pause-scene)))))
