(in-package :snake-game)

(gk:defgame snake-gamekit () 
  ()
  (:viewport-width *canvas-width*)
  (:viewport-height *canvas-height*)
  (:viewport-title "Thnake!")) 

(gk:register-resource-package 
 :snake-game
 (asdf:system-relative-pathname
  :snake-game "assets/"))

(gk:define-font 'snake-game::mono-font "LiberationMono-Regular.ttf")

(defmethod gk:act ((app snake-gamekit))
  (unless (endp *scene-stack*)
    (act (car *scene-stack*))))

(defmethod gk:draw ((app snake-gamekit))
  (gk:draw-rect *origin* *canvas-width* *canvas-height* 
                     :fill-paint *black*)
  (unless (endp *scene-stack*)
    (draw (car *scene-stack*))))

(defun score-path ()
  (asdf:system-relative-pathname :snake-game "scores.txt"))

(defun load-high-scores ()
  (if (cl-fad:file-exists-p (score-path))
      (with-open-file (eat (score-path) :direction :input)
        (sort (read eat) #'> :key #'cdr))
      (loop repeat 5 collect (cons nil 0))))

(defun write-high-scores ()
  (with-open-file (writer (score-path) :direction :output
                                       :if-exists :supersede)
    (format writer "~S" *high-scores*)))

(defun start-game ()
  (gk:start 'snake-gamekit))

(defmethod gk:post-initialize ((game snake-gamekit))
  (defparameter *high-scores* (load-high-scores))
  (setf *last-time* (get-internal-real-time))
  (defparameter *full-font* 
    (gk:make-font 'snake-game::mono-font *bz*))
  (defparameter *half-font* 
    (gk:make-font 'snake-game::mono-font (half *bz*)))
  (defparameter *double-font*
    (gk:make-font 'snake-game::mono-font (* 2 *bz*)))
  (push-scene (make-instance 'title-scene)))

(start-game)
