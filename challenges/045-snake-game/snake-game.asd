;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(pushnew :bodge-gl2 *features*)

(asdf:defsystem :snake-game
  :name "snake-game"
  :version "0.0"
  :author "arthev"
  :depends-on (trivial-gamekit)
  :components ((:file "packages")
               (:file "constants")
               (:file "utilities")
               (:file "scene-stack")
               (:file "snake")
               (:file "apple")
               (:file "title-scene")
               (:file "high-score-entry-scene")
               (:file "simple-scenes")
               (:file "snake-scene")
               (:file "main")))
