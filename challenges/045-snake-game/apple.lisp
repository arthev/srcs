(in-package :snake-game)

(defclass apple ()
  ((location
    :accessor location
    :initform (gk:vec2 (fhalf *mw*) *mh*))))

(defmethod relocate-apple ((a apple) (s snake))
  (loop while (vec-in-vec-list (location a) (tail s))
        do (setf (location a) (gk:vec2 (random *mw*) (random *mh*)))))

(defmethod draw ((a apple))
  (gk:draw-circle (gk:vec2 
                   (+ *hbz* (* *bz* (gk:x (location a))))
                   (+ *hbz* (* *bz* (gk:y (location a)))))
                  (/ *bz* 4)
                  :stroke-paint *colour*
                  :thickness (/ *bz* 8)))
