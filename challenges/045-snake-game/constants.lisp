(in-package :snake-game)


(defparameter *black* (gk:vec4 0 0 0 1))
(defparameter *transparent-black* (gk:vec4 0 0 0 0.75))
(defparameter *colour* (gk:vec4 (/ 255 255) (/ 176 255) (/ 0   255) 1))
(defparameter *red* (gk:vec4 1 0 0 1))

;;;Presumably, the dependencies between mw, mh, bz and canvas size ought
;;;to be inverted, so that mw and mh are constant, and bz a vec2
;;;calculate from the real canvas width and height, but *shrugs*.
(defparameter *canvas-width* 640)
(defparameter *canvas-height* 640)
(defparameter *origin* (gk:vec2 0 0))

(defparameter *bz* 64)
(defparameter *hbz* 32)
(defparameter *mw* (1- (/ *canvas-width* *bz*)))
(defparameter *mh* (- (/ *canvas-height* *bz*) 2))

(defparameter *alphabet* "abcdefghijklmnopqrstuvwxyz")
