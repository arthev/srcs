(in-package :snake-game)

(defclass high-score-entry-scene (base-scene)
  ((name
    :accessor name
    :initform "")
   (score
    :accessor score
    :initarg :score)
   (snake1
    :accessor snake1
    :initform (make-instance 'snake))
   (snake2
    :accessor snake2
    :initform (make-instance 'snake))))

(defmethod initialize-instance :after ((scene high-score-entry-scene) &key)
  (setf (tail (snake1 scene))
        (loop for i from *mw* above 0 collect (gk:vec2 i 0)))
  (reinit-snake-head (snake1 scene))
  (setf (tail (snake2 scene))
        (loop for i from 0 below *mw* collect (gk:vec2 i (1+ *mh*))))
  (reinit-snake-head (snake2 scene))
  (setf (direction (snake1 scene)) :left)
  (setf (direction (snake2 scene)) :right))

(defmethod when-popped ((scene high-score-entry-scene))
  (push-scene (make-instance 'high-score-scene)))

(defun hs-entry-letter (c scene)
  (setf (name scene) (concatenate 'string (name scene) c))) 

(defun hs-entry-finish (scene)
  (let* ((beatens (member (score scene) *high-scores* 
                          :key #'cdr :test #'>))
         (entry (cons (name scene) (score scene)))
         (pos (position (car beatens) *high-scores*))
         (new (if (zerop pos) (cons entry *high-scores*)
                  (append (subseq *high-scores* 0 pos)
                          (cons entry beatens)))))
    (setf *high-scores* (subseq new 0 5))
    (write-high-scores)
    (pop-scene)))

(defmethod bind-keys ((scene high-score-entry-scene))
  (map nil 
       (lambda (c)
         (bind-button 
          (intern (string-upcase (string c)) "KEYWORD")
          :pressed (lambda () (hs-entry-letter (string c) scene))))
       *alphabet*)
  (bind-button :enter :pressed
               (lambda () (hs-entry-finish scene)))
  (bind-button :backspace :pressed
               (lambda () 
                 (when (plusp (length (name scene)))
                   (setf (name scene)
                         (subseq (name scene) 
                                 0 (1- (length (name scene)))))))))
  
(defmethod draw ((scene high-score-entry-scene))
  (enforce-framerate)
  (let ((mw (half *canvas-width*))
        (mh (half *canvas-height*)))
    (center-text-at "New high-score!"
                    mw
                    (+ mh *bz*))
    (center-text-at "Enter name:"
                    mw
                    mh
                    *half-font*)
    (center-text-at (name scene)
                    mw
                    (- mh *bz*)
                    *double-font*))
  (draw (snake1 scene))
  (move-snake (snake1 scene))
  (draw (snake2 scene))
  (move-snake (snake2 scene)))
