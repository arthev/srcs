(in-package :snake-game)

(defun vec2-equal (vec1 vec2)
  (and (= (gk:x vec1) (gk:x vec2))
       (= (gk:y vec1) (gk:y vec2))))

(defun vec-in-vec-list (comp-vec vec-list)
  (loop for some-vec in vec-list
        when (and (vec2-equal comp-vec some-vec)
                  (not (eql comp-vec some-vec)))
          do (return t)
        finally (return nil)))

(defun bind-button (key state action)
  (push key (bound-keys (car *scene-stack*)))
  (gk:bind-button key state action))

(defun cind (i seq)
  (loop until (and (not (minusp i)) (< i (length seq)))
        do (if (minusp i) 
               (incf i (length seq))
               (decf i (length seq)))
        finally (return i)))

(defun half (x)
  (/ x 2))

(defun fhalf (x)
  (floor (/ x 2)))

(declaim #+sbcl(sb-ext:muffle-conditions style-warning))
(defmacro with-text-bounds (vec)
  `(multiple-value-bind (origin width height advance)
       (gk:calc-text-bounds text (if font font *full-font*))
     ,(list 'gk:draw-text 'text vec 
            :fill-color '*colour* 
            :font '(if font font *full-font*))))

(defun center-text-at (text x y &optional font)
  (with-text-bounds
      (gk:vec2 (- x (half width))
               (- y (half height)))))

(defun right-align-text-at (text x y &optional font)
  (with-text-bounds
      (gk:vec2 (- x width)
               (- y (half height)))))

(defun left-align-text-at (text x y &optional font)
  (with-text-bounds
      (gk:vec2 x
               (- y (half height)))))


(declaim #+sbcl(sb-ext:unmuffle-conditions style-warning))
(defun draw-transparent-screen ()
  (gk:draw-rect *origin* *canvas-width* *canvas-height*
                :fill-paint *transparent-black*))
