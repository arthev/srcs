(in-package :snake-game)

(defun option-text (option) (car option))
(defun option-fn (option) (cdr option))

(defclass title-scene (base-scene)
  ((options
    :accessor options
    :initform 
    (mapcar 
     #'cons 
     '(play high-score exit)
     (list
      (lambda () (push-scene (make-instance 'snake-scene)))
      (lambda () (push-scene (make-instance 'high-score-scene)))
      (lambda () (gk:stop)) )))
   (i
    :accessor i
    :initform 0))
  (:documentation "Title screen."))

(defmethod bind-keys ((scene title-scene))
  (macrolet ((shlmbd (body)
               `(lambda ()
                  (let ((ind (i scene))
                        (opts (options scene)))
                    ,body))))
    (bind-button :up :pressed
                 (shlmbd (setf (i scene) (cind (1- ind) opts))))
    (bind-button :down :pressed
                 (shlmbd (setf (i scene) (cind (1+ ind) opts))))
    (bind-button :enter :pressed
                 (shlmbd (funcall (option-fn (elt opts ind)))))))

(defmethod draw ((scene title-scene))
  (let ((wm (half *canvas-width*))
        (hc (- (half *canvas-height*) *bz*))
        (i (i scene))
        (opts (options scene)))
    (center-text-at
     "Thnake!" wm (- *canvas-height* *bz*) *double-font*)
    (center-text-at 
     (string (option-text (elt opts i)))
     wm hc)
    (center-text-at
     (string (option-text (elt opts (cind (1+ i) opts))))
     wm (- hc *bz*) *half-font*)
    (center-text-at
     (string (option-text (elt opts (cind (1- i) opts))))
     wm (+ hc *bz*) *half-font*)))
