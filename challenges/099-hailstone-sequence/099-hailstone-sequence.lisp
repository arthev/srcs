(defun hailstone-sequence (n)
  (loop while (not (= n 1))
        do (setf n (if (evenp n) (/ n 2) (1+ (* 3 n))))
        counting 1))

(defun looper (start end)
  (let ((curr 0)
        (max-s 0)
        (max-i 0))
    (loop for i from start to end
          do (setf curr (hailstone-sequence i))
          when (> curr max-s)
            do (progn (setf max-s curr) (setf max-i i))
;          do (format *query-io* "~D : ~D steps.~%" i curr)
          finally 
             (return
               (format nil "~D took ~D steps, which was longest." 
                       max-i max-s)))))
