;;;;Following pseudocode in chapter 7 of CLRS
(defparameter *quicksort-comp* #'<=)

(defun randomized-quicksort (seq left right)
  (when (< left right)
    (let ((pivoted (randomized-partition seq left right)))
      (randomized-quicksort seq left (1- pivoted))
      (randomized-quicksort seq (1+ pivoted) right))))

(defun randomized-partition (seq left right)
  (let ((i (+ left (random (- right left)))))
    (rotatef (elt seq right) (elt seq i))
    (partition seq left right)))

(defun partition (seq left right)
  (let* ((pivot (elt seq right))
         (i (1- left)))
    (loop for j from left to (1- right)
          when (funcall *quicksort-comp* (elt seq j) pivot)
            do (progn
                 (incf i)
                 (rotatef (elt seq i) (elt seq j))))
    (rotatef (elt seq (1+ i)) (elt seq right))
    (1+ i)))

(defun quicksort (seq &optional fn)
  (let ((*quicksort-comp* (if fn fn *quicksort-comp*)))
    (randomized-quicksort seq 0 (1- (length seq)))
    seq))
