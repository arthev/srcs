(defun fizzbuzz ()
  (loop for i from 1 to 100
       do (format t "~A~%"
		  (cond ((= 0 (rem i 15)) "FizzBuzz")
			((= 0 (rem i 3)) "Fizz")
			((= 0 (rem i 5)) "Buzz")
			(t i))))
  'done)
(fizzbuzz)
