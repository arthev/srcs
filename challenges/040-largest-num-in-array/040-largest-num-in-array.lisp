(defun index-of-largest (seq)
  (let ((i 0)
        (max-i 0)
        (max-val (elt seq 0)))
    (loop for x being the elements of seq
          when (> x max-val)
            do (progn (setf max-val x) (setf max-i i))
          do (incf i)
          finally (return max-i))))
