(defun vowel-p (ch)
  (find ch "aeiouyøæå" :test #'equal))

(defun count-vowels-in-string (s)
  (loop for ch across s
     with count = 0
     when (vowel-p ch)
     do (incf count)
     finally (return count)))
