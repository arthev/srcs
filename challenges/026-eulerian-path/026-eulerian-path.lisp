(defclass undirected-graph ()
  ((nodes
    :accessor nodes
    :initarg :nodes)
   (edges
    :accessor edges
    :initarg :edges)))

(defparameter graph-1
  (make-instance 
   'undirected-graph
   :nodes '(a b c)
   :edges (list (list 'a 'b) (list 'b 'c) (list 'c 'a))))

(defparameter graph-2
  (make-instance
   'undirected-graph
   :nodes '(a b c d)
   :edges (list (list 'a 'b) (list 'b 'c) (list 'b 'd))))

(defparameter graph-3
  (make-instance
   'undirected-graph
   :nodes '(a b c)
   :edges (list (list 'a 'b) (list 'b 'c))))

(defparameter graph-4
  (make-instance
   'undirected-graph
   :nodes '(a b c d e)
   :edges '((a b) (a c) (a d) (a e)
            (b c) (b d) (b e)
            (c d) (c e))))

(defgeneric connected-paths (graph node &optional edges)
  (:documentation "Gives a list of edges from node."))

(defmethod connected-paths ((graph undirected-graph) node 
                            &optional (edges nil edges-supplied-p))
  (remove-if 
   (lambda (edge) (not (member node edge)))
   (if (null edges-supplied-p) (edges graph) edges)))

(defun other-node (node edge)
  (loop for e in edge
        when (not (eql node e))
          do (return e)))

(defmethod has-eulerian-path-p ((graph undirected-graph))
  (let ((nodes-with-odd-degree nil))
    (loop for n in (nodes graph)
          when (oddp (length (connected-paths graph n)))
            do (push n nodes-with-odd-degree))
    (if (or (zerop (length nodes-with-odd-degree))
            (= 2 (length nodes-with-odd-degree)))
        (values t nodes-with-odd-degree)
        (values nil nodes-with-odd-degree))))
          
(defmethod eulerian-path ((graph undirected-graph) start)
  (when (not (member start (nodes graph)))
    (error "eulerian-path for graph w/ nodes: ~S, but start-node ~S given"
           (nodes graph) start))
  (multiple-value-bind (has-p odds) (has-eulerian-path-p graph)
    (when (or
           (not has-p)
           (and (plusp (length odds)) 
                (evenp (length (connected-paths graph start)))))
      (return-from eulerian-path nil))
    ;;Implements http://www.graph-magics.com/articles/euler.php
    (let ((edges (edges graph))
          (stack nil)
          (path nil)
          (current start))
      (loop for con-paths = (connected-paths graph current edges)
            until (and (null con-paths)
                       (null stack))
            do (if (null con-paths)
                   (progn (push current path)
                          (setf current (pop stack)))
                   (progn (push current stack)
                          (let* ((edge (first con-paths))
                                 (neighbour (other-node current edge)))
                            (setf edges (remove edge edges))
                            (setf current neighbour))))
            finally (push start path))
      path)))
    
;;;It would now be fairly simple to make a separate directed-graph class,
;;;implement methods (and algo for directed graphs) and reuse other-node.
;;;Also, I inadvertently assumed no edges from a node v to itself.
;;;Adding support for that would primary require 'shaving' those off from
;;;consideration during has-eulerian-path-p.
