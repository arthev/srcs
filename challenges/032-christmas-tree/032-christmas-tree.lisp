(defun generate-tree (height)
  (assert (plusp height) (height)
          "generate-tree requires plusp height: ~S received" height)
  (loop for i from 1 to height
        do (format t "~,,v@A~{~A~}~%" 
                   (- height i)
                   #\ 
                   (loop for j from 1 to (1- (* 2 i)) collect #\@)))
  (loop repeat 2 do (format t "~,,v@A~%" height #\*)))

