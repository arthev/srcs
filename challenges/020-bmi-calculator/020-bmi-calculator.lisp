(defun weight-height->bmi (weight height)
  (let ((m-height (/ height 100)))
    (coerce (/ weight (expt m-height 2)) 'float)))

(defun prompt (s)
  (format t s)
  (force-output t)
  (read))

(let ((w (prompt "Please enter weight in kg: "))
      (h (prompt "Please enter height in cm: ")))
  (format t "You have a bmi of ~F.~%" (weight-height->bmi w h)))
