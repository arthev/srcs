(format t "100! : ~A~%"
        (let ((p 1))
          (loop for i from 2 to 100
                do (setf p (* i p)))
          p))
