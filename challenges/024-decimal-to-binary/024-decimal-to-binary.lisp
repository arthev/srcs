;; ;;;;Searching around to see if CL supports out of the box (well, it
;; ;;;;does for printing), and came across the following tidy recursive func:
;; (defun decimal->binary (n)
;;   (labels 
;;       ((recurser (n)
;;          (multiple-value-bind (a r) (floor n 2)
;;            (if (zerop n) nil (cons r (recurser a))))))
;;     (reverse (recurser n))))

;; ;;So, since finding decimal->binary somewhere took care of that aspect,
;; ;;here's binary->decimal instead for the fun of it!
;; (defun binary->decimal (b)
;;   (labels
;;       ((recurser (b)
;;          (if (endp b)
;;              0
;;              (+ (car b) (* 2 (recurser (cdr b)))))))
;;     (recurser (reverse b))))
(load "~/quicklisp/setup.lisp")
(ql:quickload :personal-utilities)

(defun decimal->binary (n)
  (reverse (cl-arthur:recurser (n) (n)
             (multiple-value-bind (a r) (floor n 2)
               (if (zerop n) nil (cons r (:recurser a)))))))

(defun binary->decimal (b)
  (cl-arthur:recurser (b) ((reverse b))
    (if (endp b)
        0
        (+ (car b) (* 2 (:recurser (cdr b)))))))
