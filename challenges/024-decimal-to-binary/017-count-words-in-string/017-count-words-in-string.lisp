(load "~/quicklisp/setup.lisp")
(ql:quickload :personal-utilities)

;; (defun split (str &optional (symb #\ ))
;;   (labels 
;;       ((recurser (s)
;;          (let ((pos (position symb s)))
;;            (if (not pos)
;;                (list s)
;;                (cons (subseq s 0 pos)
;;                      (recurser (subseq s (1+ pos))))))))
;;     (remove "" (recurser str) :test #'string=)))

(defun split (str &optional (symb #\ ))
  (remove "" 
          (cl-arthur:recurser (s) (str)
            (let ((pos (position symb s)))
              (if (not pos)
                  (list s)
                  (cons (subseq s 0 pos)
                        (:recurser (subseq s (1+ pos)))))))
          :test
          #'string=))

(defun count-words-in-string (str)
  (length (split str)))
