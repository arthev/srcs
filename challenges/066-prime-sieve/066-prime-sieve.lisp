(defun prime-numbers-up-to (n)
  (let ((found (list 2)))
    (loop for i from 3 to n
          for sqrti = (sqrt i)
          for primep = (loop for num in found
                           when (> num sqrti)
                             do (return t)
                           when (zerop (rem i num))
                             do (return nil)
                           finally (return t))
          when primep
            do (setf found (append found (list i))))
    found))
