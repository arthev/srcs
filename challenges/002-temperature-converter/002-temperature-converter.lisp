(defun fahrenheit->celsius (F)
  (* (- F 32) (/ 5 9)))

(defun celsius->fahrenheit (C)
  (+ 32 (* C (/ 9 5))))
