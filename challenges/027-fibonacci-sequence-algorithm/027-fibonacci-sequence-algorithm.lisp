;Recursion. Θ(n^2)
(defun fibonacci-r (n)
  (cond ((zerop n) 0)
	((<= n 2) 1)
	(t (+ (fibonacci (- n 1))
	      (fibonacci (- n 2))))))

;Iteration. Θ(n)
(defun fibonacci-i (n)
  (cond ((zerop n) 0)
	((<= n 2) 1)
	(t (loop for i from 3 to n
	      for f1 = 1 then f2
	      and f2 = 1 then (+ f2 f1)
	      finally (return (+ f2 f1))))))

;Memoized iterative fibonacci
(defparameter *fib-vector* (make-array 100 :fill-pointer 0 :adjustable t))
(vector-push-extend 0 *fib-vector*)
(vector-push-extend 1 *fib-vector*)
(vector-push-extend 1 *fib-vector*)
(defun fibonacci-m (n)
  (defun fib-stored (m)
    (if (< m (fill-pointer *fib-vector*))
	(elt *fib-vector* m)
	nil))
  (if (fib-stored n)
      (fib-stored n)
      (loop for i from (1- (fill-pointer *fib-vector*)) to n
	 for f1 = (elt *fib-vector* i) 
	 then f2
	 and f2 = (+ (elt *fib-vector* i) (elt *fib-vector* (1- i))) 
	 then (+ f2 f1)
	 do (vector-push-extend f2 *fib-vector*)
	 finally (return (+ f2 f1)))))
