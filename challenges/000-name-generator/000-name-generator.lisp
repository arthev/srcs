;Utility
(defun mappend (fn lst)
  "Apply fn to each elt in lst and append results."
  (apply #'append (mapcar fn lst)))

(defun random-elt (choices)
  "Choose an element from a list at random random"
  (elt choices (random (length choices))))
(defun one-of (set)
  "Pick one elt of a set and make a list of it"
  (list (random-elt set)))

(defun list-to-string (lst)
  (format nil "~{~A~}" lst))


;Program
(defparameter *forename-grammar*
  '((start -> (start-syl start-syl) (start-syl middle* end-syl))
    (middle* -> () (binding-vowel middle-syl middle*) (binding-vowel double-syl middle*))
    (end-syl -> lk dict bos ros lm)
    (start-syl -> voi "nil" ark myr se)
    (middle-syl -> elk vlk hij ras rot)
    (binding-vowel -> i a u e)
    (double-syl -> tottot attur melkor zagbags))
  "Rules for silly forenames")

(defparameter *surname-grammar*
  '((start -> (silly-start middle* end-syl) (middle* end-syl))
    (middle* -> () (grunt-sound middle-syl grunt-sound middle*) (middle-syl))
    (end-syl -> yeh jup jaja heh izer 2000)
    (silly-start -> mc don wuggie snuggy cud bun)
    (grunt-sound -> agh ugh grrr hngh hnje)
    (middle-syl -> ara gorn leg lass fro do sam gam gee gand alf))
  "Rules for very silly surnames.")

(defvar *grammar* *forename-grammar*
  "The grammar used by generate. Initially *forename-grammar* but
   can switch dynamically...")

(defun rule-lhs (rule)
  "left-hand-side of a rule"
  (car rule))
(defun rule-rhs (rule)
  "right-hand-side of a rule"
  (cddr rule))
(defun rewrites (category)
  "Return a list of possible transformations for this category"
  (rule-rhs (assoc category *grammar*)))

(defun generate (phrase)
  "Generate a random phrase over a grammar."
  (cond ((listp phrase)
	 (mappend #'generate phrase))
	((rewrites phrase)
	 (generate (random-elt (rewrites phrase))))
	(t (list phrase))))
	 
(defun generate-name ()
  (defun generate-forename ()
    "Generate a random forename through grammar and generate."
    (let ((*grammar* *forename-grammar*))
      (string-capitalize (list-to-string (generate 'start)))))
  (defun generate-surname ()
    "Generate a random surname through grammar and generate."
    (let ((*grammar* *surname-grammar*))
      (string-capitalize (list-to-string (generate 'start)))))
  "Generate a full name."
  (concatenate 'string (generate-forename) " " (generate-surname)))

(loop for i from 0 to (parse-integer (or (cadr sb-ext:*posix-argv*) "0"))
   do (format t "~A~%" (generate-name)))

