(in-package :utils)

(defparameter *whitespace-chars*
  (vector #\Space #\Newline #\Backspace #\Tab 
          #\Linefeed #\Page #\Return #\Rubout))

(defun normalize-word (str &key (trim-whitespace t))
  (let ((normalized (string-trim *whitespace-chars* str)))
    (cond ((not trim-whitespace) str)
          ((string= "" normalized) nil)
          (t normalized))))

(defun position-options (sequence options)
  (loop for i from 0 below (length sequence)
        when (find (elt sequence i) options)
          do (return-from position-options i)
        finally (return nil)))

(defun split-options (str options &key (trim-whitespace t))
  (let ((result nil))
    (do* ((str str (subseq str (1+ pos)))
          (pos (position-options str options)
               (position-options str options)))
         ((not pos) 
          (push 
           (normalize-word str :trim-whitespace trim-whitespace) 
           result))
      (push 
       (normalize-word (subseq str 0 pos) :trim-whitespace trim-whitespace) 
       result))
    (nreverse (remove nil result))))

(defun split-on-whitespace (str)
  (split-options str *whitespace-chars* :trim-whitespace t))

(defun prompt-s (str)
  (format *query-io* "~a: " str)
  (force-output *query-io*)
  (read *query-io*))

(defun prompt-l (str)  (format *query-io* "~A: " str)
  (force-output *query-io*)
  (read-line *query-io*))

(defun prompt-i (str)
  (parse-integer (prompt-l str) :junk-allowed t))

(defun copy-2D-array (array)
  (let* ((x (array-dimension array 0))
         (y (array-dimension array 1))
         (new (make-array `(,x ,y))))
    (loop for c from 0 below x
          do (loop for r from 0 below y
                   do (setf (aref new c r) (aref array c r))))
    new))

(defmacro rec ((&rest params) (&rest call-vals) &body body)
  (let ((ln (gensym)))
    (labels 
        ((replace-recurser-with-ln (sexp)
           (cond ((and (atom sexp) (eql sexp :rec)) ln)
                 ((atom sexp) sexp)
                 (t (mapcar #'replace-recurser-with-ln sexp)))))
      (let ((new-body (replace-recurser-with-ln body)))
        `(labels
             ((,ln ,params
                ,@new-body))
           (,ln ,@call-vals))))))

;;;:Utilities from On Lisp
;;;Short list utilities - fig. 4.1
(declaim (inline last1 singlep append1 nconc1 mklist))

(defun last1 (lst)
  (car (last lst)))

(defun singlep (lst)
  (and (consp lst) (not (cdr lst))))

(defun append1 (lst obj)
  (append lst (list obj)))

(defun nconc1 (lst obj)
  (nconc lst (list obj)))

(defun mklist (obj)
  (if (listp obj) obj (list obj)))

;;;Long list utilities - fig. 4.2
(defun longer-p (x y)
  (labels ((compare (x y)
             (and (consp x)
                  (or (null y)
                      (compare (cdr x) (cdr y))))))
    (if (and (listp x ) (listp y))
        (compare x y)
        (> (length x) (length y)))))

(defun filter (fn lst)
  (let ((acc nil))
    (dolist (x lst)
      (let ((val (funcall fn x)))
        (when val (push val acc))))
    (nreverse acc)))

(defun group (source n)
  (when (zerop n) (error "zero length"))
  (if (not source)
      nil
      (rec (source acc) (source nil)
        (let ((rest (nthcdr n source)))
          (if (consp rest)
              (:rec rest (cons (subseq source 0 n) acc))
              (nreverse (cons source acc)))))))

;;;Doubly recursive list utilities - fig. 4.3
(defun flatten (x)
  (rec (x acc) (x nil)
    (cond ((null x) acc)
          ((atom x) (cons x acc))
          (t (:rec (car x) (:rec (cdr x) acc))))))

(defun prune (test tree)
  (rec (tree acc) (tree nil)
    (cond ((null tree) (nreverse acc))
          ((consp (car tree)) (:rec (cdr tree)
                                    (cons (:rec (car tree) nil) acc)))
          (t (:rec (cdr tree)
                   (if (funcall test (car tree))
                       acc
                       (cons (car tree) acc)))))))

;;;List search utilities - fig. 4.4
(defun find2 (fn lst)
  (if (null lst)
      nil
      (let ((val (funcall fn (car lst))))
        (if val
            (values (car lst) val)
            (find2 fn (cdr lst))))))

(defun before (x y lst &key (test #'eql))
  (and lst
       (let ((first (car lst)))
         (cond ((funcall test y first) nil)
               ((funcall test x first) lst)
               (t (before x y (cdr lst) :test test))))))

(defun after (x y lst &key (test #'eql))
  (let ((rest (before y x lst :test test)))
    (and rest (member x rest :test test))))

(defun duplicate (obj lst &key (test #'eql))
  (member obj (cdr (member obj lst :test test)) :test test))

(defun split-if (fn lst)
  (let ((acc nil))
    (do ((src lst (cdr src)))
        ((or (null src) (funcall fn (car src)))
         (values (nreverse acc) src))
      (push (car src) acc))))

;;;List search utils that comp. elts - fig. 4.5
(defun most (fn lst)
  (if (null lst)
      (values nil nil)
      (let* ((wins (car lst))
             (max (funcall fn wins)))
        (dolist (obj (cdr lst))
          (let ((score (funcall fn obj)))
            (when (> score max)
              (setq wins obj
                    max score))))
        (values wins max))))

(defun best (fn lst)
  (if (null lst)
      nil
      (let ((wins (car lst)))
        (dolist (obj (cdr lst))
          (if (funcall fn obj wins)
              (setq wins obj)))
        wins)))

(defun mostn (fn lst)
  (if (null lst)
      (values nil nil)
      (let ((result (list (car lst)))
            (max (funcall fn (car lst))))
        (dolist (obj (cdr lst))
          (let ((score (funcall fn obj)))
            (cond ((> score max) (setq max score
                                       result (list obj)))
                  ((= score max) (push obj result)))))
        (values (nreverse result) max))))

;;;Mapping utils - fig. 4.6
(defun map0-n (fn n)
  (mapa-b fn 0 n))

(defun map1-n (fn n)
  (mapa-b fn 1 n))

(defun mapa-b (fn a b &optional (step 1))
  (do ((i a (+ i step))
       (result nil))
      ((> i b) (nreverse result))
    (push (funcall fn i) result)))

(defun map-> (fn start test-fn succ-fn)
  (do ((i start (funcall succ-fn i))
       (result nil))
      ((funcall test-fn i) (nreverse result))
    (push (funcall fn i) result)))

(defun mappend (fn &rest lsts)
  (apply #'append (apply #'mapcar fn lsts)))

(defun mapcars (fn &rest lsts)
  (let ((result nil))
    (dolist (lst lsts)
      (dolist (obj lst)
        (push (funcall fn obj) result)))
    (nreverse result)))

(defun rmapcar (fn &rest args)
  (if (some #'atom args)
      (apply fn args)
      (apply #'mapcar
             (lambda (&rest args)
               (apply #'rmapcar fn args))
             args)))

;;;IO utils - fig. 4.7
(defun readlist (&rest args)
  (values (read-from-string
           (concatenate 'string
                        "("
                        (apply #'read-line args)
                        ")"))))

;;prompt - skip
;;breakloop - skip

;;;Symbols and strings - fig. 4.8
(defun mkstr (&rest args)
  (with-output-to-string (s)
    (dolist (a args) (princ a s))))

(defun symb (&rest args)
  (values (intern (apply #'mkstr args))))

(defun reread (&rest args)
  (values (read-from-string (apply #'mkstr args))))

(defun explode (sym)
  (map 'list (lambda (c)
               (intern (make-string 1 :initial-element c)))
       (symbol-name sym)))

;;;;Chapter 5
;;;Returning destructive equivalents - fig. 5.1
(defvar *!equivs* (make-hash-table))

(defun ! (fn)
  (or (gethash fn *!equivs*) fn))

(defun def! (fn fn!)
  (setf (gethash fn *!equivs*) fn!))

;;;Memoizing utility - fig. 5.2
(defun memoize (fn)
  (let ((cache (make-hash-table :test #'equal)))
    (lambda (&rest args)
      (multiple-value-bind (val win) (gethash args cache)
        (if win
            val
            (setf (gethash args cache) (apply fn args)))))))

;;;Composition - fig. 5.3
(defun compose (&rest fns)
  (if fns
      (let ((fn1 (car (last fns)))
            (fns (butlast fns)))
        (lambda (&rest args)
          (reduce #'funcall fns
                  :from-end t
                  :initial-value (apply fn1 args))))
      #'identity))

;;;function combination - fig. 5.4
(defun fif (if then &optional else)
  (lambda (x)
    (if (funcall if x)
        (funcall then x)
        (if else (funcall else x)))))

(defun fint (fn &rest fns)
  (if (null fns)
      fn
      (let ((chain (apply #'fint fns)))
        (lambda (x)
          (and (funcall fn x) (funcall chain x))))))

(defun fun (fn &rest fns)
  (if (null fns)
      fn
      (let ((chain (apply #'fun fns)))
        (lambda (x)
          (or (funcall fn x) (funcall chain x))))))

;;;recursers - fig. 5.5
;;;see fig. 5.6 for example use
(defun lrec (rec &optional base)
  (labels ((self (lst)
             (if (null lst)
                 (if (functionp base)
                     (funcall base)
                     base)
                 (funcall rec
                          (car lst)
                          (lambda () (self (cdr lst)))))))
    #'self))

;;;fig. 5.8 (see 5.9 for example use)
(defun ttrav (rec &optional (base #'identity))
  (labels ((self (tree)
             (if (atom tree)
                 (if (functionp base)
                     (funcall base tree)
                     base)
                 (funcall rec
                          (self (car tree))
                          (if (cdr tree) (self (cdr tree)))))))
    #'self))

;;;fig. 5.10
(defun trec (rec &optional (base #'identity))
  (labels
      ((self (tree)
         (if (atom tree)
             (if (functionp base)
                 (funcall base tree)
                 base)
             (funcall rec
                      tree
                      (lambda () (self (car tree)))
                      (lambda () (if (cdr tree) (self (cdr tree))))))))
    #'self))


;;;Chapter 7 - macros!
;;;Fig. 7.5
(defmacro mac (expr)
  `(pprint (macroexpand-1 ',expr)))

;;;Chapter 11 - classic macros
;;;bind macros - Fig. 11.2
(defmacro when-bind ((var expr) &body body)
  `(let ((,var ,expr))
     (when ,var
       ,@body)))

(defmacro when-bind* (binds &body body)
  (if (null binds)
      `(progn ,@body)
      `(let (,(car binds))
         (if ,(caar binds)
             (when-bind* ,(cdr binds) ,@body)))))

(defmacro with-gensyms (syms &body body)
  `(let ,(mapcar #'(lambda (s) `(,s (gensym))) syms)
     ,@body))

;;;condlet - Fig. 11.3
;;;NOTE : doesn't work with Graham's example run?
(defmacro condlet (clauses &body body)
  (let ((bodfn (gensym))
	(vars (loop for v in (remove-duplicates
			      (mapcar #'car (mappend #'cdr clauses)))
		    collect (cons v (gensym)))))
    `(labels ((,bodfn ,(mapcar #'car vars)
	       ,@body))
      (cond ,@(loop for cl in clauses
		    collect (condlet-clause vars cl bodfn))))))

(defun condlet-clause (vars cl bodfn)
  `(,(car cl) (let ,(mapcar #'cdr vars)
		(let ,(condlet-binds vars cl)
		  (,bodfn ,@(mapcar #'cdr vars))))))

(defun condlet-binds (vars cl)
  (mapcar #'(lambda (bindform)
              (if (consp bindform)
                  (cons (cdr (assoc (car bindform) vars))
                        (cdr bindform))))
          (cdr cl)))

;;;conditional evaluations - Fig. 11.5
(defmacro if3 (test t-case nil-case ?-case)
  `(case ,test
     ((nil) ,nil-case)
     (?     ,?-case)
     (t     ,t-case)))

(defmacro nif (expr pos zero neg)
  (let ((g (gensym)))
    `(let ((,g ,expr))
       (cond ((plusp ,g) ,pos)
             ((zerop ,g) ,zero)
             (t ,neg)))))

;;;Fig. 11.6
(defmacro in (obj &rest choices)
  (let ((insym (gensym)))
    `(let ((,insym ,obj))
       (or ,@(mapcar #'(lambda (c) `(eql ,insym ,c))
                     choices)))))

(defmacro inq (obj &rest args)
  `(in ,obj ,@(mapcar #'(lambda (a) `',a) args)))

(defmacro in-if (fn &rest choices)
  (let ((fnsym (gensym)))
    `(let ((,fnsym ,fn))
       (or ,@(mapcar #'(lambda (c) `(funcall ,fnsym ,c))
           choices)))))

(defmacro >case (expr &rest clauses)
  (let ((g (gensym)))
    `(let ((,g ,expr))
       (cond ,@(mapcar #'(lambda (cl) (>casex g cl)) clauses)))))

(defun >casex (g cl)
  (let ((key (car cl)) (rest (cdr cl)))
    (cond ((consp key) `((in ,g ,@key) ,@rest))
;          ((inq key t otherwise) `(t ,@rest))
          (t (error "bad >case clause")))))

;;;Iteration macros - Fig. 11.7
(defmacro while (test @body body)
  `(do ()
       ((not ,test))
     ,@body))

(defmacro till (test @body body)
  `(do ()
       (,test)
     ,@body))

(defmacro for ((var start stop) &body body)
  (let ((gstop (gensym)))
    `(do ((,var ,start (1+ var))
          (,gstop ,stop))
         ((> ,var ,gstop))
       ,@body)))

;;;Fig. 11.8
(defmacro do-tuples/o (parms source &body body)
  (when parms
    (let ((src (gensym)))
      `(prog ((,src ,source))
          (mapc #'(lambda ,parms ,@body)
                ,@(map0-n #'(lambda (n)
                              `(nthcdr ,n ,src))
                          (1- (length parms))))))))

(defmacro do-tuples/c (parms source &body body)
  (when parms
    (with-gensyms (src rest bodfn)
      (let ((len (length parms)))
        `(let ((,src ,source))
           (when (nthcdr ,(1- len) ,src)
             (labels ((,bodfn ,parms ,@body))
               (do ((,rest ,src (cdr ,rest)))
                   ((not (nthcdr ,(1- len) ,rest))
                    ,@(mapcar #'(lambda (args)
                                  `(,bodfn ,@args))
                              (dt-args len rest src))
                    nil)
                 (,bodfn ,@(map1-n #'(lambda (n)
                                       `(nth ,(1- n) ,rest))
                                   len))))))))))

(defun dt-args (len rest src)
  (map0-n #'(lambda (m)
              (map1-n #'(lambda (n)
                          (let ((x (+ m n)))
                            (if (>= x len)
                                `(nth ,(- x len) ,src)
                                `(nth ,(1- x) ,rest))))
                      len))
          (- len 2)))

;;;Iteration with multiple values
;;;Fig. 1.10
(defmacro mvdo* (parm-cl test-cl &body body)
  (mvdo-gen parm-cl parm-cl test-cl body))

(defun mvdo-gen (binds rebinds test body)
  (if (null binds)
      (let ((label (gensym)))
        `(prog nil
            ,label
            (when ,(car test)
              (return (progn ,@(cdr test))))
            ,@body
            ,@(mvdo-rebind-gen rebinds)
            (go ,label)))
      (let ((rec (mvdo-gen (cdr binds) rebinds test body)))
        (let ((var/s (caar binds))
              (expr (cadar binds)))
          (if (atom var/s)
              `(let ((,var/s ,expr)) ,rec)
              `(multiple-value-bind ,var/s ,expr ,rec))))))

(defun mvdo-rebind-gen (rebinds)
  (cond ((null rebinds) nil)
        ((< (length (car rebinds)) 3) (mvdo-rebind-gen (cdr rebinds)))
        (t (cons (list (if (atom (caar rebinds)) 'setq 'multiple-value-setq)
                       (caar rebinds)
                       (third (car rebinds)))
                 (mvdo-rebind-gen (cdr rebinds))))))

;;;Fig 1.12
(defmacro mvpsetq (&rest args)
  (let* ((pairs (group args 2))
         (syms (mapcar (lambda (p)
                         (mapcar (lambda (x) (gensym))
                                 (mklist (car p))))
                       pairs)))
    (labels ((rec (ps ss)
               (if (null ps)
                   `(setq
                     ,@(mapcan (lambda (p s)
                                 (shuffle (mklist (car p))
                                          s))
                               pairs syms))
                   (let ((body (rec (cdr ps) (cdr ss))))
                     (let ((var/s (caar ps))
                           (expr (cadar ps)))
                       (if (consp var/s)
                           `(multiple-value-bind ,(car ss) ,expr ,body)
                           `(let ((,@(car ss) ,expr))
                              ,body)))))))
      (rec pairs syms))))

(defun shuffle (x y)
  (cond ((null x) y)
        ((null y) x)
        (t (list* (car x) (car y)
                  (shuffle (cdr x) (cdr y))))))

;;;Fig 1.13
(defmacro mvdo (binds (test &rest result) &body body)
  (let ((label (gensym))
        (temps (mapcar (lambda (b)
                         (if (listp (car b))
                             (mapcar (lambda (x)
                                       (gensym))
                                     (car b))
                             (gensym)))
                       binds)))
    `(let ,(mappend #'mklist temps)
       (mvpsetq ,@(mapcan (lambda (b var)
                            (list var (cadr b)))
                          binds
                          temps))
       (prog ,(mapcar (lambda (b var) (list b var))
               (mappend #'mklist (mapcar #'car binds))
               (mappend #'mklist temps))
          ,label
          (when ,test (return (progn ,@result)))
          ,@body
          (mvpsetq ,@(mapcan (lambda (b)
                               (when (third b)
                                 (list (car b) (third b))))
                             binds))
          (go ,label)))))

;;;Chapter 12
;;;Fig. 12.1
(define-modify-macro toggle2 () not)

(defmacro toggle (&rest args)
  `(progn
     ,@(mapcar (lambda (a) `(toggle2 ,a))
               args)))

(defmacro allf (val &rest args)
  (with-gensyms (gval)
    `(let ((,gval ,val))
       (setf ,@(mapcar (lambda (a) (list a gval))
                       args)))))

(defmacro nilf (&rest args) `(allf nil ,@args))

(defmacro tf (&rest args) `(allf t ,@args))

;;;Fig. 12.2
(define-modify-macro concf (obj) nconc)

(define-modify-macro conc1f (obj) conc1)

(define-modify-macro concnew (obj &rest args)
  (lambda (place obj &rest args)
    (unless (apply #'member obj place args)
      (nconc place (list obj)))))

;;;Fig. 12.3
(defmacro _f (op place &rest args)
  (multiple-value-bind (vars forms var set access) (get-setf-method place)
    `(let* (,@(mapcar #'list vars forms)
            (,(car var) (,op ,access ,@args)))
       ,set)))

(defmacro pull (obj place &rest args)
  (multiple-value-bind (vars forms var set access) (get-setf-method place)
    (let ((g (gensym)))
      `(let* ((,g ,obj)
              ,@(mapcar #'list vars forms)
              (,(car var) (delete ,g ,access ,@args)))
         ,set))))

(defmacro pull (test place &rest args)
  (multiple-value-bind (vars forms var set access) (get-setf-method place)
    (let ((g (gensym)))
      `(let* ((,g ,test)
              ,@(mapcar #'list vars forms)
              (,(car var) (delete-if ,g ,access ,@args)))
         ,set))))

(defmacro popn (n place)
  (multiple-value-bind (vars forms var set access) (get-setf-method place)
    (with-gensyms (gn glst)
      `(let* ((,gn ,n)
              ,@(mapcar #'list vars forms)
              (,glst ,access)
              (,(car var) (nthcdr ,gn ,glst)))
         (prog1 (subseq ,glst 0 ,gn)
           ,set)))))

;;;Fig. 12.4 - warning: bubblesorts! O(n^2)
(defmacro sortf (op &rest places)
  (let* ((meths (mapcar (lambda (p)
                          (multiple-value-list
                           (get-setf-method p)))
                        places))
         (temps (apply #'append (mapcar #'third meths))))
    `(let* ,(mapcar #'list
                    (mapcan (lambda (m)
                              (append (first m) (third m)))
                            meths)
                    (mapcan (lambda (m)
                              (append (second m) (list (fifth m))))
                            meths))
       ,@(mapcon (lambda (rest)
                   (mapcar
                    (lambda (arg)
                      `(unless (,op ,(car rest) ,arg)
                         (rotatef ,(car rest) ,arg)))
                    (cdr rest)))
                 temps)
       ,@(mapcar #'fourth meths))))

;;;Chapter 12.5 contains some interesting discussion regarding asymmetric inversions

;;;Chapter 13 - compile time computation. Not included due to the constraint on using w/ apply, etc.
;;;Good to know about the technique, however.

;;;Chapter 14 - anaphoric macros
;;;Fig. 14.1
(defmacro aif (test-form then-form &optional else-form)
  `(let ((it ,test-form))
     (if it ,then-form ,else-form)))

(defmacro awhen (test-form &body body)
  `(aif ,test-form (progn ,@body)))

(defmacro awhile (expr &body body)
  `(do ((it ,expr ,expr))
       ((not it))
     ,@body))

(defmacro aand (&rest args)
  (cond ((null args) t)
        ((null (cdr args)) (car args))
        (t `(aif ,(car args) (aand ,@(cdr args))))))

(defmacro acond (&rest clauses)
  (if (null clauses)
      nil
      (let ((cl1 (car clauses))
            (sym (gensym)))
        `(let ((,sym ,(car cl1)))
           (if ,sym
               (let ((it ,sym)) ,@(cdr cl1))
               (acond ,@(cdr clauses)))))))

;;;Fig. 14.2
(defmacro alambda (parms &body body)
  `(labels ((self ,parms ,@body))
     #'self))

(defmacro ablock (tag &rest args)
  `(block ,tag
     ,(funcall (alambda (args)
                 (case (length args)
                   (0 nil)
                   (1 (car args))
                   (t `(let ((it ,(car args)))
                         ,(self (cdr args))))))
               args)))

;;;Fig. 14.3 - multiple value anaphoric macros (for dealing with nil)
(defmacro aif2 (test &optional then else)
  (let ((win (gensym)))
    `(multiple-value-bind (it ,win) ,test
       (if (or it ,win) ,then ,else))))

(defmacro awhen2 (test &body body)
  `(aif2 ,test
         (progn ,@body)))

(defmacro awhile2 (test &body body)
  (let ((flag (gensym)))
    `(let ((,flag t))
       (while ,flag
              (aif2 ,test
                    (progn ,@body)
                    (setq ,flag nil))))))

(defmacro acond2 (&rest clauses)
  (if (null clauses)
      nil
      (let ((cl1 (car clauses))
            (val (gensym))
            (win (gensym)))
        `(multiple-value-bind (,val ,win) ,(car cl1)
           (if (or ,val ,win)
               (let ((it ,val)) ,@(cdr cl1))
               (acond2 ,@(cdr clauses)))))))

;;;Fig. 14.4 - file utils
(let ((g (gensym)))
  (defun read2 (&optional (str *standard-input*))
    (let ((val (read str nil g)))
      (unless (equal val g) (values val t)))))

(defmacro do-file (filename &body body)
  (let ((str (gensym)))
    `(with-open-file (,str ,filename)
       (awhile2 (read2 ,str)
                ,@body))))

;;;Chapter 15 - macros returning fns
;;;Fig. 15.1
(defmacro fn (expr) `#',(rbuild expr))

(defun rbuild (expr)
  (if (or (atom expr) (eq (car expr) 'lambda))
      expr
      (if (eq (car expr) 'compose)
          (build-compose (cdr expr))
          (build-call (car expr) (cdr expr)))))

(defun build-call (op fns)
  (let ((g (gensym)))
    `(lambda (,g)
       (,op ,@(mapcar (lambda (f)
                        `(,(rbuild f) ,g))
                      fns)))))

(defun build-compose (fns)
  (let ((g (gensym)))
    `(lambda (,g)
       ,(labels ((rec (fns)
                   (if fns
                       `(,(rbuild (car fns))
                         ,(rec (cdr fns)))
                       g)))
          (rec fns)))))

;;;Fig. 15.2
(defmacro alrec (rec &optional base)
  (let ((gfn (gensym)))
    `(lrec #'(lambda (it ,gfn)
               (symbol-macrolet ((rec (funcall ,gfn)))
                 ,rec))
           ,base)))

(defmacro on-cdrs (rec base &rest lsts)
  `(funcall (alrec ,rec #'(lambda () ,base)) ,@lsts))

;;;Fig. 15.4 - on-cdrs utils
(defun maxmin (args)
  (when args
    (on-cdrs (multiple-value-bind (mx mn) rec
               (values (max mx it) (min mn it)))
             (values (car args) (car args))
             (cdr args))))

;;;Fig. 15.5 - tree rec macros
(defmacro atrec (rec &optional (base 'it))
  (let ((lfn (gensym)) (rfn (gensym)))
    `(trec #'(lambda (it ,lfn ,rfn)
               (symbol-macrolet ((left (funcall ,lfn))
                                 (right (funcall ,rfn)))
                 ,rec))
           #'(lambda (it) ,base))))

(defmacro on-trees (rec base &rest trees)
  `(funcall (atrec ,rec ,base) ,@trees))

;;;Ch. 15.4 contains a brief discussion of lazy evaluation w/ force & delay
;;;Useful to comp. with when I find myself in need of it!


;;;Chapter 16 - macro-defining macros!
;;;Fig. 16.1 & 16.2. Discussion in text re. templating multiquote design.
(defmacro abbrev (short long)
  `(defmacro short (&rest args)
     `(,',long ,@args)))

(defmacro abbrevs (&rest names)
  `(progn
     ,@(mapcar (lambda (pair)
                 `(abbrev ,@pair))
               (group names 2))))

(defmacro propmacro (propname)
  `(defmacro ,propname (obj)
     `(get ,obj ',',propname)))

(defmacro propmacros (&rest props)
  `(progn
     ,@(mapcar (lambda (p) `(propmacro ,p))
               props)))

;;;Fig. 16.3 - a+ and alist
(defmacro a+ (&rest args)
  (a+expand args nil))

(defun a+expand (args syms)
  (if args
      (let ((sym (gensym)))
        `(let* ((,sym ,(car args))
                (it, sym))
           ,(a+expand (cdr args)
                      (append syms (list sym)))))
      `(+ ,@syms)))

(defmacro alist (&rest args)
  (alist-expand args nil))

(defun alist-expand (args syms)
  (if args
      (let ((sym (gensym)))
        `(let* ((,sym ,(car args))
                (it ,sym))
           ,(alist-expand (cdr args)
                          (append syms (list sym)))))
      `(list ,@syms)))

;;;Fig. 16.5
(defmacro defanaph (name &key calls (rule :all))
  (let* ((opname (or calls (pop-symbol name)))
         (body (case rule
                 (:all    `(anaphex1 args `(,opname)))
                 (:first  `(anaphex2 ',opname args))
                 (:place  `(anaphex3 ',opname args)))))
    `(defmacro ,name (&rest args)
       ,body)))

(defun anaphex1 (args expr)
  (if args
      (let ((sym (gensym)))
        `(let* ((,sym ,(car args))
                (it ,sym))
           ,(anaphex1 (cdr args)
                     (append expr (list sym)))))
      expr))

(defun anaphex2 (op args)
  `(let ((it ,(car args))) (,op it ,@(cdr args))))

(defun anaphex3 (op args)
  `(_f (lambda (it) (,op it ,@(cdr args))) ,(car args)))

(defun pop-symbol (sym)
  (intern (subseq (symbol-name sym) 1)))

(defanaph asetf :rule :place)

;;;Fig. 17.4 describes a macro for defining delimiter read-macros
(defmacro defdelim (left right parms &body body)
  `(ddfn ,left ,right #'(lambda ,parms ,@body)))

(let ((rpar (get-macro-character #\) )))
  (defun ddfn (left right fn)
    (set-macro-character right rpar)
    (set-dispatch-macro-character
     #\# left
     #'(lambda (stream char1 char2)
         (apply fn (read-delimited-list right stream t))))))

;;;Chapter 18
;;;Fig. 18.1 - general sequence destructuring operator
(defmacro dbind (pat seq &body body)
  (let ((gseq (gensym)))
    `(let ((,gseq ,seq))
       ,(dbind-ex (destruc pat gseq #'atom) body))))

(defun destruc (pat seq &optional (atom? #'atom) (n 0))
  (if (null pat)
      nil
      (let ((rest (cond ((funcall atom? pat) pat)
                        ((eq (car pat) '&rest) (cadr pat))
                        ((eq (car pat) '&body) (cadr pat))
                        (t nil))))
        (if rest
            `((,rest (subseq ,seq ,n)))
            (let ((p (car pat))
                  (rec (destruc (cdr pat) seq atom? (1+ n))))
              (if (funcall atom? p)
                  (cons `(,p (elt ,seq ,n))
                        rec)
                  (let ((var (gensym)))
                    (cons (cons `(,var (elt ,seq ,n))
                                (destruc p var atom?))
                          rec))))))))

(defun dbind-ex (binds body)
  (if (null binds)
      `(progn ,@body)
      `(let ,(mapcar #'(lambda (b)
                         (if (consp (car b))
                             (car b)
                             b))
              binds)
         ,(dbind-ex (mapcan (lambda (b)
                              (when (consp (car b))
                                (cdr b)))
                            binds)
                    body))))
                    
;;;Fig. 18.2 - destructuring on arrays
(defmacro with-matrix (pats ar &body body)
  (let ((gar (gensym)))
    `(let ((,gar ,ar))
       (let ,(let ((row -1))
               (mapcan (lambda (pat)
                         (incf row)
                         (setq col -1)
                         (mapcar (lambda (p)
                                   `(,p (aref ,gar
                                              ,row
                                              ,(incf col))))
                                 pat))
                       pats))
         ,@body))))

(defmacro with-array (pat ar &body body)
  (let ((gar (gensym)))
    `(let ((,gar ,ar))
       (let ,(mapcar (lambda (p)
                       `(,(car p) (aref ,gar ,@(cdr p))))
                     pat)
         ,@body))))
                     
;;;Fig. 18.3 - destructuring on structures
(defmacro with-struct ((name . fields) struct &body body)
  (let ((gs (gensym)))
    `(let ((,gs ,struct))
       (let ,(mapcar (lambda (f)
                       `(,f (,(symb name f) ,gs)))
                     fields)
         ,@body))))

;;;Fig. 18.4 is a 'version' of dbind (with-places) for setfable places.

;;;Ch. 18.4 describes matching utils


;;;Ch. 20 - continuations
;;;Fig. 20.4
(setq *cont* #'identity)

(defmacro =lambda (parms &body body)
  `#'(lambda (*cont* ,@parms) ,@body))

(defmacro =defun (name parms &body body)
  (let ((f (intern (concatenate 'string "=" (symbol-name name)))))
    `(progn
       (defmacro ,name ,parms
         `(,',f *cont* ,,@parms))
       (defun ,f (*cont* ,@parms) ,@body))))

(defmacro =bind (parms expr &body body)
  `(let ((*cont* #'(lambda ,parms ,@body))) ,expr))

(defmacro =values (&rest retvals)
  `(funcall *cont* ,@retvals))

(defmacro =funcall (fn &rest args)
  `(funcall ,fn *cont* ,@args))

(defmacro =apply (fn &rest args)
  `(apply ,fn *cont* ,@args))

;;;Fig. 20.5 list restrictions imposed by the above continuation macros.

;;;Chapter 22
;;;Fig. 22.5 - nondet operators
(defparameter *paths* nil)
(defconstant failsym '@)

(defmacro choose (&rest choices)
  (if choices
      `(progn
         ,@(mapcar (lambda (c)
                     `(push (lambda () ,c) *paths*))
                   (reverse (cdr choices)))
         ,(car choices))
      `(fail)))

(defmacro choose-bind (var choices &body body)
  `(cb (lambda (,var) ,@body) ,choices))

(defun cb (fn choices)
  (if choices
      (progn
        (when (cdr choices)
          (push (lambda () (cb fn (cdr choices))) *paths*))
        (funcall fn (car choices)))
      (fail)))

(defun fail ()
  (if *paths*
      (funcall (pop *paths*))
      failsym))

;;;A true, correct choose - in Scheme - is presented in Fig. 22.14.
;;;It simply uses BFS instead of DFS.






    



                        











;;;TODO:
;; Whenever a macro which does intentional variable capture is exported to
;; another package, it is necessary also to export the symbol being captured. For
;; example, wherever aif is exported, it should be as well. Otherwise the it which
;; appears in the macro definition would be a different symbol from an it used in a
;; macro call.
(intern it)
(intern self)
;;?









          














(defun re-pu ()
  (let ((pack (find-package :utils)))
    (do-all-symbols (sym pack)
      (when (eql (symbol-package sym) pack) (export sym)))))
(re-pu)
