;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(asdf:defsystem :utils
  :name "personal utilities"
  :version "0.0"
  :author "arthev"
  :components ((:file "packages")
               (:file "utils")))
             

