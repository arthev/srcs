(defpackage :com.gigamonkeys.web
  (:use :cl :net.aserve :com.gigamonkeys.html))

(in-package :com.gigamonkeys.web)

(start :port 2001)

(defun show-query-params (request entity)
  (with-http-response (request entity :content-type "text/html")
    (with-http-body (request entity)
      (with-html-output ((request-reply-stream request))
        (html
          (:standard-page
           (:title "Query Parameters")
           (if (request-query request)
               (html
                 (:table :border 1
                         (loop for (k . v) in (request-query request)
                               do (html (:tr (:td k) (:td v))))))
               (html (:p "No query parameters.")))))))))
(publish :path "/show-query-params" :function 'show-query-params)

(defun simple-form (request entity)
  (with-http-response (request entity :content-type "text/html")
    (with-http-body (request entity)
      (html
        (:html
          (:head (:title "Simple Form"))
          (:body
           (:form :method "POST" :action "/show-query-params"
                    (:table
                     (:tr (:td "Foo")
                          (:td (:input :name "foo" :size 20)))
                     (:tr (:td "Password")
                          (:td (:input :name "password" :type "password" :size 20))))
                    (:p (:input :name "submit" :type "submit" :value "Okay")
                        (:input ::type "reset" :value "Reset")))))))))
(publish :path "/simple-form" :function 'simple-form)


(defun random-number (request entity)
  (with-http-response (request entity :content-type "text/html")
    (with-http-body (request entity)
      (let* ((*html-output* (request-reply-stream request))
             (limit-string (or (request-query-value "limit" request) ""))
             (limit (or (parse-integer limit-string :junk-allowed t) 1000)))
        (html
          (:html
            (:head (:title "Random"))
            (:body
             (:p "Random number: " (:print (random limit))))))))))
